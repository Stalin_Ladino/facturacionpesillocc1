package com.ec.gpf.ingresos.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mapeo tabla IN_TIPOS_VALES
 * @author Kruger_SL
 *
 */
@Entity
@Table(name = "IN_DIFERENCIAS_CAJA")
@NamedQuery(name = "InDiferenciasCaja.findAll", query = "SELECT i FROM InDiferenciasCaja i")
public class InDiferenciasCaja implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@EmbeddedId
	private InDiferenciasCajaPK inDiferenciasCajaPK;
		
	@Column(name = "REGISTRADO")
	private BigDecimal registrado;
	
	@Column(name = "REMITIDO")
	private BigDecimal remitido;
	
	@Column(name = "FALTANTE")
	private BigDecimal faltante;
	
	@Column(name = "SOBRANTE")
	private BigDecimal sobrante;
		
	@Column(name = "MANUAL", length = 1)
	private String manual;
	
	@Column(name = "OBSERVACIONES", length = 800)
	private String observaciones;
	
	@Column(name = "ESTADO")
	private Long estado;
	
	@Column(name = "CSINO1", length = 1)
	private String csino1;
	
	@Column(name = "CSINO", length = 1)
	private String csino;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CFECHA")
	private Date cfecha;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CFECHA1")
	private Date cfecha1;
	
	@Column(name="CCHAR",length=500)
	private String cchar;
	
	@Column(name="CCHAR1",length=500)
	private String cchar1;

	@Column(name="CNUMBER1")
	private BigDecimal cnumber1;
	
	@Column(name="CNUMBER")
	private BigDecimal cnumber;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;
	
	@Column(name="USUARIO",length=30)
	private String usuario;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_BAJA")
	private Date fechaBaja;
	
	//bi-directional many-to-one association to InDifcajaVale
	@OneToMany(mappedBy="inDiferenciasCaja",fetch = FetchType.LAZY)
	private List<InDifCajaVales> inDifcajaVales;

	
	public BigDecimal getRegistrado() {
		return registrado;
	}

	public void setRegistrado(BigDecimal registrado) {
		this.registrado = registrado;
	}

	public BigDecimal getRemitido() {
		return remitido;
	}

	public void setRemitido(BigDecimal remitido) {
		this.remitido = remitido;
	}

	public BigDecimal getFaltante() {
		return faltante;
	}

	public void setFaltante(BigDecimal faltante) {
		this.faltante = faltante;
	}

	public BigDecimal getSobrante() {
		return sobrante;
	}

	public void setSobrante(BigDecimal sobrante) {
		this.sobrante = sobrante;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public String getCsino1() {
		return csino1;
	}

	public void setCsino1(String csino1) {
		this.csino1 = csino1;
	}

	public String getCsino() {
		return csino;
	}

	public void setCsino(String csino) {
		this.csino = csino;
	}

	public Date getCfecha() {
		return cfecha;
	}

	public void setCfecha(Date cfecha) {
		this.cfecha = cfecha;
	}

	public Date getCfecha1() {
		return cfecha1;
	}

	public void setCfecha1(Date cfecha1) {
		this.cfecha1 = cfecha1;
	}

	public String getCchar() {
		return cchar;
	}

	public void setCchar(String cchar) {
		this.cchar = cchar;
	}

	public String getCchar1() {
		return cchar1;
	}

	public void setCchar1(String cchar1) {
		this.cchar1 = cchar1;
	}

	public BigDecimal getCnumber1() {
		return cnumber1;
	}

	public void setCnumber1(BigDecimal cnumber1) {
		this.cnumber1 = cnumber1;
	}

	public BigDecimal getCnumber() {
		return cnumber;
	}

	public void setCnumber(BigDecimal cnumber) {
		this.cnumber = cnumber;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public List<InDifCajaVales> getInDifcajaVales() {
		return inDifcajaVales;
	}

	public void setInDifcajaVales(List<InDifCajaVales> inDifcajaVales) {
		this.inDifcajaVales = inDifcajaVales;
	}

	public InDiferenciasCajaPK getInDiferenciasCajaPK() {
		return inDiferenciasCajaPK;
	}

	public void setInDiferenciasCajaPK(InDiferenciasCajaPK inDiferenciasCajaPK) {
		this.inDiferenciasCajaPK = inDiferenciasCajaPK;
	}

	
}