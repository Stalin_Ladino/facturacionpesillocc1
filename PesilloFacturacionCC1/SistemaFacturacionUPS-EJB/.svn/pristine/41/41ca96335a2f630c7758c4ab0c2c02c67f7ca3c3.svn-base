package com.ec.gpf.administracion.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import com.ec.gpf.ingresos.jpa.entidades.InFarmaciasIngreso;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the AD_FARMACIAS database table.
 * 
 */
@Entity
@Table(name="AD_FARMACIAS")
@NamedQuery(name="AdFarmacia.findAll", query="SELECT a FROM AdFarmacia a")
public class AdFarmacia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long codigo;

	@Column(name="AUTORIZACION_FARMACEUTICA")
	private String autorizacionFarmaceutica;

	private String calle;

	private BigDecimal campo1;

	private BigDecimal campo2;

	private String campo3;

	private String campo4;

	@Column(name="CENTRO_COSTO")
	private String centroCosto;

	@Column(name="CENTRO_COSTOS_VITALCARD")
	private BigDecimal centroCostosVitalcard;

	private BigDecimal ciudad;

	@Column(name="CODIGO_ANTIGUO")
	private String codigoAntiguo;

	@Column(name="CODIGO_RADIO_SHACK")
	private BigDecimal codigoRadioShack;

	@Column(name="DATABASE_SID")
	private String databaseSid;

	@Column(name="EMPRESA")
	private Long empresa;

	@Column(name="FACTOR_SANA")
	private BigDecimal factorSana;

	@Column(name="FMA_AUTORIZACION_FARMACEUTICA")
	private String fmaAutorizacionFarmaceutica;

	private String interseccion;

	@Column(name="MAXIMO_DCTO_SANA")
	private BigDecimal maximoDctoSana;

	@Column(name="MINIMO_DCTO_SANA")
	private BigDecimal minimoDctoSana;
	
	@Column(name="NOMBRE")
	private String nombre;

	private String numero;

	@Column(name="NUMERO_RUC")
	private String numeroRuc;

	private String telefono;

	@Column(name="TIPO_FARMACIA")
	private BigDecimal tipoFarmacia;

	private BigDecimal zona;

	
	@OneToMany(mappedBy = "adFarmacia", fetch = FetchType.LAZY)
	private List<InFarmaciasIngreso> inFarmaciasIngreso;
	
	public AdFarmacia() {
	}

	public long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getAutorizacionFarmaceutica() {
		return this.autorizacionFarmaceutica;
	}

	public void setAutorizacionFarmaceutica(String autorizacionFarmaceutica) {
		this.autorizacionFarmaceutica = autorizacionFarmaceutica;
	}

	public String getCalle() {
		return this.calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public BigDecimal getCampo1() {
		return this.campo1;
	}

	public void setCampo1(BigDecimal campo1) {
		this.campo1 = campo1;
	}

	public BigDecimal getCampo2() {
		return this.campo2;
	}

	public void setCampo2(BigDecimal campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return this.campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return this.campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCentroCosto() {
		return this.centroCosto;
	}

	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}

	public BigDecimal getCentroCostosVitalcard() {
		return this.centroCostosVitalcard;
	}

	public void setCentroCostosVitalcard(BigDecimal centroCostosVitalcard) {
		this.centroCostosVitalcard = centroCostosVitalcard;
	}

	public BigDecimal getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(BigDecimal ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodigoAntiguo() {
		return this.codigoAntiguo;
	}

	public void setCodigoAntiguo(String codigoAntiguo) {
		this.codigoAntiguo = codigoAntiguo;
	}

	public BigDecimal getCodigoRadioShack() {
		return this.codigoRadioShack;
	}

	public void setCodigoRadioShack(BigDecimal codigoRadioShack) {
		this.codigoRadioShack = codigoRadioShack;
	}

	public String getDatabaseSid() {
		return this.databaseSid;
	}

	public void setDatabaseSid(String databaseSid) {
		this.databaseSid = databaseSid;
	}

	public Long getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}

	public BigDecimal getFactorSana() {
		return this.factorSana;
	}

	public void setFactorSana(BigDecimal factorSana) {
		this.factorSana = factorSana;
	}

	public String getFmaAutorizacionFarmaceutica() {
		return this.fmaAutorizacionFarmaceutica;
	}

	public void setFmaAutorizacionFarmaceutica(String fmaAutorizacionFarmaceutica) {
		this.fmaAutorizacionFarmaceutica = fmaAutorizacionFarmaceutica;
	}

	public String getInterseccion() {
		return this.interseccion;
	}

	public void setInterseccion(String interseccion) {
		this.interseccion = interseccion;
	}

	public BigDecimal getMaximoDctoSana() {
		return this.maximoDctoSana;
	}

	public void setMaximoDctoSana(BigDecimal maximoDctoSana) {
		this.maximoDctoSana = maximoDctoSana;
	}

	public BigDecimal getMinimoDctoSana() {
		return this.minimoDctoSana;
	}

	public void setMinimoDctoSana(BigDecimal minimoDctoSana) {
		this.minimoDctoSana = minimoDctoSana;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroRuc() {
		return this.numeroRuc;
	}

	public void setNumeroRuc(String numeroRuc) {
		this.numeroRuc = numeroRuc;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public BigDecimal getTipoFarmacia() {
		return this.tipoFarmacia;
	}

	public void setTipoFarmacia(BigDecimal tipoFarmacia) {
		this.tipoFarmacia = tipoFarmacia;
	}

	public BigDecimal getZona() {
		return this.zona;
	}

	public void setZona(BigDecimal zona) {
		this.zona = zona;
	}

	public List<InFarmaciasIngreso> getInFarmaciasIngreso() {
		return inFarmaciasIngreso;
	}

	public void setInFarmaciasIngreso(List<InFarmaciasIngreso> inFarmaciasIngreso) {
		this.inFarmaciasIngreso = inFarmaciasIngreso;
	}

}