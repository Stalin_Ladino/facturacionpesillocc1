package com.ec.gpf.ingresos.jpa.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The primary key class for the IN_DIFERENCIAS_CAJA database table.
 * 
 */
@Embeddable
public class InDiferenciasCajaPK implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3340420251980472328L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false, precision=12)
	private Long farmacia;

	@Column(insertable=false, updatable=false, unique=true, nullable=false, precision=12)
	private Long empleado;

	@Temporal(TemporalType.DATE)
	@Column(unique=true, nullable=false)
	private Date fecha;

	@Column(unique=true, nullable=false, precision=12)
	private Long secuencia;

	public InDiferenciasCajaPK() {
	}
	public Long getFarmacia() {
		return this.farmacia;
	}
	public void setFarmacia(Long farmacia) {
		this.farmacia = farmacia;
	}
	public Long getEmpleado() {
		return this.empleado;
	}
	public void setEmpleado(Long empleado) {
		this.empleado = empleado;
	}
	public Date getFecha() {
		return this.fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Long getSecuencia() {
		return this.secuencia;
	}
	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof InDiferenciasCajaPK)) {
			return false;
		}
		InDiferenciasCajaPK castOther = (InDiferenciasCajaPK)other;
		return 
			(this.farmacia == castOther.farmacia)
			&& (this.empleado == castOther.empleado)
			&& this.fecha.equals(castOther.fecha)
			&& (this.secuencia == castOther.secuencia);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.farmacia ^ (this.farmacia >>> 32)));
		hash = hash * prime + ((int) (this.empleado ^ (this.empleado >>> 32)));
		hash = hash * prime + this.fecha.hashCode();
		hash = hash * prime + ((int) (this.secuencia ^ (this.secuencia >>> 32)));
		
		return hash;
	}
}