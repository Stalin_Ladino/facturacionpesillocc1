/*
 * Copyright 2013 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <b> Validador de campos personalizado. </b>
 * 
 * @author mmontaguano
 * @version $Revision: 1.0 $
 *          <p>
 *          [$Author: mmontaguano $, $Date: 25/11/2013 $]
 *          </p>
 */
public class Validador {

	public static Boolean validarCedulaRuc(String cedulaRuc, Boolean validaRuc) throws NumberFormatException {
		boolean cedulaRucCorrecta = false;
		if (!validaRuc) {
			return true;
		}
		if (cedulaRuc.length() == 10) {
			cedulaRucCorrecta = validarCedula(cedulaRuc);
		} else if (cedulaRuc.length() > 10) {
			int tercerDigito = Integer.parseInt(cedulaRuc.substring(2, 3));
			if (tercerDigito < 6) {
				// RUC PERSONA NATURAL
				if (validarCedula(cedulaRuc.substring(0, 10))) {
					if (cedulaRuc.substring(10).equals("001")) {
						cedulaRucCorrecta = true;
					} else {
						cedulaRucCorrecta = false;
					}
				}

			} else if (tercerDigito == 6) {
				// RUC EMPRESAS PUBLICAS
				if (validarRucEmpresaPublica(cedulaRuc)) {
					if (cedulaRuc.substring(10).equals("001")) {
						cedulaRucCorrecta = true;
					} else {
						cedulaRucCorrecta = false;
					}
				}
			} else if (tercerDigito == 9) {
				// RUC JURIDICA O EXTRANJERA
				if (validarRucJuridicosExtrajeros(cedulaRuc)) {
					if (cedulaRuc.substring(10).equals("001")) {
						cedulaRucCorrecta = true;
					} else {
						cedulaRucCorrecta = false;
					}
				}
			}
		} else {
			cedulaRucCorrecta = false;
		}
		return cedulaRucCorrecta;
	}

	public static Boolean validarCedula(String cedula) throws NumberFormatException {
		// ConstantesApp.LongitudCedula
		int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
		if (tercerDigito < 6) {
			// Coeficientes de validacion cedula
			// El decimo digito se lo considera digito verificador
			int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
			int verificador = Integer.parseInt(cedula.substring(9, 10));
			int suma = 0;
			int digito = 0;
			for (int i = 0; i < (cedula.length() - 1); i++) {
				digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
				suma += ((digito % 10) + (digito / 10));
			}
			if ((suma % 10 == 0) && (suma % 10 == verificador)) {
				return true;
			} else if ((10 - (suma % 10)) == verificador) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private static Boolean validarRucEmpresaPublica(String ruc) {
		boolean resp = false;
		// boolean val = false;
		Integer v1, v2, v3, v4, v5, v6, v7, v8, v9;
		Integer sumatoria;
		Integer modulo;
		Integer digito;
		int[] d = new int[ruc.length()];

		for (int i = 0; i < d.length; i++) {
			d[i] = Integer.parseInt(ruc.charAt(i) + "");
		}

		v1 = d[0] * 3;
		v2 = d[1] * 2;
		v3 = d[2] * 7;
		v4 = d[3] * 6;
		v5 = d[4] * 5;
		v6 = d[5] * 4;
		v7 = d[6] * 3;
		v8 = d[7] * 2;
		v9 = d[8];

		sumatoria = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8;
		modulo = sumatoria % 11;
		if (modulo == 0) {
			return true;
		}
		digito = 11 - modulo;

		if (digito.equals(v9)) {
			resp = true;
		} else {
			resp = false;
		}
		return resp;
	}

	public static Boolean validarRucJuridicosExtrajeros(String ruc) {
		boolean resp_dato = false;

		int[] d = new int[10];
		int[] coeficientes = { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
		int constante = 11;
		int suma = 0;

		for (int i = 0; i < d.length; i++) {
			d[i] = Integer.parseInt(ruc.charAt(i) + "");
		}

		for (int i = 0; i < d.length - 1; i++) {
			d[i] = d[i] * coeficientes[i];
			suma += d[i];
		}

		int aux, resp;

		aux = suma % constante;
		resp = constante - aux;

		resp = (aux == 0) ? 0 : resp;

		if (resp == d[9]) {
			resp_dato = true;
		} else {
			resp_dato = false;
		}
		return resp_dato;
	}

	/*
	 * Metodo para validar correo electronio
	 * 
	 * @param String correo
	 */
	public static Boolean validarCorreo(String correo) {
		Pattern pat = null;
		Matcher mat = null;
		pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		if(correo != null){
			mat = pat.matcher(correo);
			if (mat.find()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Metodo para validar que las direcciones de correo electronio sean validas
	 * 
	 * @param String correo
	 */
	public static boolean validarCorreosElectronicos(String pCorreosElectronicos) {
		boolean correosCorrectos = Boolean.FALSE;
		boolean correoCorrecto = Boolean.FALSE;
		
		try {
			String[] correos = pCorreosElectronicos.split(ConstantesUtil.PUNTO_Y_COMA);
			
			for (String correo : correos) {
				correoCorrecto = validarCorreo(correo);
				if (!correoCorrecto) {
					break;
				}
			}
			
			if (correoCorrecto) {
				correosCorrectos = Boolean.TRUE;
				
			} else {
				correosCorrectos = Boolean.FALSE;
			}
			
		} catch (Exception e) {
			correosCorrectos = Boolean.FALSE;
		}
		
		return correosCorrectos;
	}
}
