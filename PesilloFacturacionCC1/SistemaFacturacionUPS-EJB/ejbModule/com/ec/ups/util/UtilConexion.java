/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ec.ups.util;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *Clase Utilitaria para identificar la base a conectarse
 * @author sladino
 */
@Stateless
public class UtilConexion {

	@PersistenceContext (unitName = "WebApplicationUPS-EJB")
	public EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}


	
}
