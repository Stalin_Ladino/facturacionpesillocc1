package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblEvento;

@Local
public interface eventosDTOInterface {
	
	void create(TblEvento TblEvento);

	void edit(TblEvento TblEvento);

	void remove(TblEvento TblEvento);

	public TblEvento findById(Integer id);
	
	public List<TblEvento> findAll();
	
}
