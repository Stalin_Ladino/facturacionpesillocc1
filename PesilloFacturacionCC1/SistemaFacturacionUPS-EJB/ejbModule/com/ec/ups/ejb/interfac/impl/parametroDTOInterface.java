package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblParametro;

@Local
public interface parametroDTOInterface {
	
	void create(TblParametro TblParametro);

	void edit(TblParametro TblParametro);

	void remove(TblParametro TblParametro);

	public TblParametro findById(Integer id);
	
	public List<TblParametro> findAll();
	
	public List<TblParametro> buscarParamSec();
	
	public TblParametro findByIdLogin(Integer id);
	
	public List<TblParametro> buscarParamBySect(Integer idSect);
	
	public List<TblParametro> buscarParamBySectServ(Integer idSect,Integer idServ);
	
}
