package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblExcedente;
import com.ec.ups.util.UtilConexion;

@Stateless
public class excedenteDTOImpl implements excedenteDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblExcedente TblExcedente) {
		utilFacade.getEm().persist(TblExcedente);
	}

	@Override
	public void edit(TblExcedente TblExcedente) {
		utilFacade.getEm().merge(TblExcedente);
		
	}

	@Override
	public void remove(TblExcedente TblExcedente) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblExcedente));
		
	}
	
//	
	public TblExcedente findById(Integer id){
		TblExcedente rol= new TblExcedente();
		Query query=utilFacade.getEm().createQuery("Select rol from TblExcedente as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (TblExcedente) query.getSingleResult();
		return rol;
	}


	public List<TblExcedente> buscarIdParam(Integer IdParam){

		List<TblExcedente> list= new ArrayList<TblExcedente>();
		try{
		Query query=utilFacade.getEm().createQuery("Select exc from TblExcedente as exc"
												 + " where idParam=:idParam");
		query.setParameter("idParam", IdParam);
		list= query.getResultList();

		}catch(NoResultException e){
			list=null;
		}
		return list;
	}
	
	
	public List<TblExcedente> findAll(){
		List<TblExcedente> sect= new ArrayList<TblExcedente>();
		Query query=utilFacade.getEm().createQuery("Select sect from TblExcedente as sect");
		sect= query.getResultList();
		return sect;
	}
	

	public TblExcedente findByIdLogin(Integer id){
		TblExcedente rol= new TblExcedente();
		Query query=utilFacade.getEm().createQuery("Select login from TblExcedente as login "
				+ " where idLogin=:id ");
		query.setParameter("id", id);
		rol= (TblExcedente) query.getSingleResult();
		return rol;
	}
	
}
