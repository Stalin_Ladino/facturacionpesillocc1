package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblSectorUsuario;

@Local
public interface sectorUsuarioDTOInterface {
	
	void create(TblSectorUsuario TblSectorUsuario);

	void edit(TblSectorUsuario TblSectorUsuario);

	void remove(TblSectorUsuario TblSectorUsuario);

	public List<TblSectorUsuario> buscarSectorUsuario(Integer idSec);
	
	public List<TblSectorUsuario> buscarPorCedula(String cedula);
	
	public List<TblSectorUsuario> buscarLogin(Integer idLogin);
	
	public List<TblSectorUsuario> findAll();

	public void eliminarDelSec(Integer idUser,Integer idSec);
	
	public List<TblSectorUsuario> buscarUser(Integer idUser);
	
	public List<TblSectorUsuario> findBySector(Integer idSec);
	
	public void eliminarDelSec(Integer idSec);
	
	
}
