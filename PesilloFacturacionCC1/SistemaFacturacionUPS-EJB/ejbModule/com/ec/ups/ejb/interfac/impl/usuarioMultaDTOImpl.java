package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.util.UtilConexion;

@Stateless
public class usuarioMultaDTOImpl implements usuarioMultaDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblUsuarioMulta tblUsuarioMulta) {
		utilFacade.getEm().persist(tblUsuarioMulta);
	}

	@Override
	public void edit(TblUsuarioMulta tblUsuarioMulta) {
		utilFacade.getEm().merge(tblUsuarioMulta);

	}

	@Override
	public void remove(TblUsuarioMulta tblUsuarioMulta) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(tblUsuarioMulta));

	}

	@Override
	public List<TblUsuarioMulta> findAll() {
		List<TblUsuarioMulta> lisMul = new ArrayList<TblUsuarioMulta>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipMul from TblUsuarioMulta as tipMul");
		lisMul = query.getResultList();
		return lisMul;

	}

	@Override
	public List<TblUsuarioMulta> findBySector(Integer idSec) {
		List<TblUsuarioMulta> lisMul = new ArrayList<TblUsuarioMulta>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipMul from TblUsuarioMulta as tipMul,"
									+ " usuarioDTO as user,"
									+ " TblMulta as tblMulta"
									+ " where tipMul.idUsu=user.idUsu"
									+ " and tblMulta.idMulta=tipMul.idMulta"
									+ " and tblMulta.idSec=:idSec");
		
		query.setParameter("idSec", idSec);
		lisMul = query.getResultList();
		return lisMul;
	}

	@Override
	public List<TblUsuarioMulta> findBySectorUser(Integer idSec, Integer idUser) {
		List<TblUsuarioMulta> lisMul = new ArrayList<TblUsuarioMulta>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipMul from TblUsuarioMulta as tipMul,"
						+ " usuarioDTO as user,"
						+ " TblSectorUsuario as tblSecUser"
						+ " where tipMul.idUsu=user.idUsu"
						+ " and tblSecUser.idSec=:idSec"
						+ " and tblSecUser.idUsu=user.idUsu"
						+ " and tipMul.idUsu=:idUser");
		query.setParameter("idSec", idSec);
		query.setParameter("idUser", idUser);
		lisMul = query.getResultList();
		return lisMul;
	}

	@Override
	public List<TblUsuarioMulta> findBySectorUserFecha(Integer idSec,
			Integer idUser, String fechIni, String fechFin) {
		
		List<TblUsuarioMulta> lisMul = new ArrayList<TblUsuarioMulta>();
		Query query = utilFacade.getEm().createQuery(
				
				"Select tipMul from TblUsuarioMulta as tipMul,"
						+ " usuarioDTO as user,"
						+ " TblSectorUsuario as tblSecUser"
						+ " where tipMul.idUsu=user.idUsu"
						+ " and tblSecUser.idSec=:idSec"
						+ " and tblSecUser.idUsu=user.idUsu"
						+ " and tipMul.idUsu=:idUser"
						+ " and tipMul.fechaReg BETWEEN '"+fechIni+"' and '"+fechFin+"'");
						
						
		query.setParameter("idSec", idSec);
		query.setParameter("idUser", idUser);
		lisMul = query.getResultList();
		return lisMul;
	}

}
