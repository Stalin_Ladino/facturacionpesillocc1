package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblRegional;
import com.ec.ups.util.UtilConexion;

@Stateless
public class regionalDTOImpl implements regionalDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblRegional TblRegional) {
		TblRegional.setIdReg(null);
		utilFacade.getEm().persist(TblRegional);
	}

	@Override
	public void edit(TblRegional TblRegional) {
		utilFacade.getEm().merge(TblRegional);
		
	}

	@Override
	public void remove(TblRegional TblRegional) {
		try {
			utilFacade.getEm().remove(utilFacade.getEm().merge(TblRegional));
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
//	
	public TblRegional findById(Integer id){
		TblRegional user= new TblRegional();
		Query query=utilFacade.getEm().createQuery("Select regional from TblRegional as regional where idReg=:id ");
		query.setParameter("id", id);
		user= (TblRegional) query.getSingleResult();
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TblRegional> findAll(){
		List<TblRegional> listUser= new ArrayList<TblRegional>();
		Query query=utilFacade.getEm().createQuery("Select regional from TblRegional as regional order by regional.nomReg");
		
		listUser= query.getResultList();
		return listUser;
	}
	

//	@SuppressWarnings("unchecked")
//	public List<TblRegional> findUserAll(){
//		List<TblRegional> listUser= new ArrayList<TblRegional>();
//		
//		Query query=utilFacade.getEm().createQuery("Select user from TblRegional as user");
//		listUser= query.getResultList();
//		return listUser;
//	}

	
}
