package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblUsuarioMulta;

@Local
public interface usuarioMultaDTOInterface {
	
	void create(TblUsuarioMulta TblConsumo);

	void edit(TblUsuarioMulta TblConsumo);

	void remove(TblUsuarioMulta TblConsumo);
	
	public List<TblUsuarioMulta> findBySector(Integer idSec);
	
	public List<TblUsuarioMulta> findAll();
	
	public List<TblUsuarioMulta> findBySectorUser(Integer idSec,Integer idUser);
	
	public List<TblUsuarioMulta> findBySectorUserFecha(Integer idSec,
			Integer idUser, String fechIni, String fechFin);

	
	
}
