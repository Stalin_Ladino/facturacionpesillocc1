package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblParametro;
import com.ec.ups.util.UtilConexion;

@Stateless
public class parametroDTOImpl implements parametroDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblParametro TblParametro) {
		TblParametro.setSecComDTO(null);
		utilFacade.getEm().persist(TblParametro);
	}

	@Override
	public void edit(TblParametro TblParametro) {
		TblParametro.setSecComDTO(null);
		utilFacade.getEm().merge(TblParametro);
		
	}

	@Override
	public void remove(TblParametro TblParametro) {
		TblParametro.setSecComDTO(null);
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblParametro));
		
	}
	
//	
	public TblParametro findById(Integer id){
		TblParametro rol= new TblParametro();
		Query query=utilFacade.getEm().createQuery("Select rol from TblParametro as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (TblParametro) query.getSingleResult();
		return rol;
	}

	public List<TblParametro> buscarParamSec(){

		List<TblParametro> sect= new ArrayList<TblParametro>();
		Query query=utilFacade.getEm().createQuery("Select param from TblParametro as param,"
												 + " secComDTO as sec,TblServicio as serv"
												 + " where param.idSec=sec.idSec"
												 + " and serv.idServ=param.idServ");
		sect=query.getResultList();
		return sect;
	}
	
	
	public List<TblParametro> buscarParamBySect(Integer idSect){

		List<TblParametro> sect= new ArrayList<TblParametro>();
		Query query=utilFacade.getEm().createQuery("Select param from TblParametro as param,"
												 + " secComDTO as sec,TblServicio as serv"
												 + " where param.idSec=sec.idSec"
												 + " and serv.idServ=param.idServ "
												 + " and param.idSec=:idSect");
		query.setParameter("idSect", idSect);
		sect=query.getResultList();
		return sect;
	}
	
	public List<TblParametro> findAll(){
		List<TblParametro> sect= new ArrayList<TblParametro>();
		Query query=utilFacade.getEm().createQuery("Select param from TblParametro as param");
		sect= query.getResultList();
		return sect;
	}
	

	public TblParametro findByIdLogin(Integer id){
		TblParametro rol= new TblParametro();
		Query query=utilFacade.getEm().createQuery("Select login from TblParametro as login "
				+ " where idLogin=:id ");
		query.setParameter("id", id);
		rol= (TblParametro) query.getSingleResult();
		return rol;
	}
	
	public List<TblParametro> buscarParamBySectServ(Integer idSect,Integer idServ){
		List<TblParametro> param= new ArrayList<TblParametro>();
		Query query=utilFacade.getEm().createQuery("Select param from TblParametro as param,"
												 + " secComDTO as sec,TblServicio as serv"
												 + " where param.idSec=sec.idSec"
												 + " and serv.idServ=param.idServ "
												 + " and serv.idServ=:idServ"
												 + " and param.idSec=:idSect");
		query.setParameter("idSect", idSect);
		query.setParameter("idServ", idServ);
		param=query.getResultList();
		return param;
	}
	
}
