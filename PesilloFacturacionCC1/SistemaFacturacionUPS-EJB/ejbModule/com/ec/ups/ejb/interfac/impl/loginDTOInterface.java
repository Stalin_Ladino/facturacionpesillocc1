package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;

@Local
public interface loginDTOInterface {
	
	void create(loginDTO loginDTO);

	void edit(loginDTO loginDTO);

	void remove(loginDTO loginDTO);

	public loginDTO findById(Integer id);
	
	public List<loginDTO> findAll();
	
	public loginDTO buscarLogin(String nombreUs, String pass);
	
	public loginDTO findByIdLogin(Integer id);
	
	public loginDTO buscarLoginOperador(String nombreUs, String pass);

	public loginDTO buscarLoginNombre(String nomUser);
	
}
