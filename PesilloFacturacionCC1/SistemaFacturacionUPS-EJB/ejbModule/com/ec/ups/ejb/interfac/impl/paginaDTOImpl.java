package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.FkRolPagina;
import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.util.UtilConexion;

@Stateless
public class paginaDTOImpl implements paginaDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblPagina TblPagina) {
		utilFacade.getEm().persist(TblPagina);
	}

	@Override
	public void createRolPagina(FkRolPagina fkRolPagina) {
		utilFacade.getEm().persist(fkRolPagina);
	}

	@Override
	public void edit(TblPagina TblPagina) {
		utilFacade.getEm().merge(TblPagina);

	}

	@Override
	public void remove(TblPagina TblPagina) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblPagina));

	}

	@Override
	public void removeRolPagina(FkRolPagina removeRolPagina) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(removeRolPagina));

	}

	//
	public List<TblPagina> findAllByLevel(Integer id) {
		List<TblPagina> pagList = new ArrayList<TblPagina>();
		Query query = utilFacade.getEm().createQuery(
				"Select pag from TblPagina as pag,FkRolPagina as fkr"
						+ " where fkr.id.idValuenivel=:id "
						+ " and fkr.id.idPag=pag.idPag");
		query.setParameter("id", id);

		pagList = query.getResultList();
		return pagList;
	}

	public TblPagina buscarLogin(String nombreUs, String pass) {

		TblPagina login = new TblPagina();
		try {
			Query query = utilFacade.getEm().createQuery(
					"Select login from TblPagina as login "
							+ " where nomUser=:nomUser"
							+ " and passUser=:passUser ");
			query.setParameter("nomUser", nombreUs);
			query.setParameter("passUser", pass);
			login = (TblPagina) query.getSingleResult();

		} catch (NoResultException e) {
			login = null;
		}
		return login;
	}

	public List<TblPagina> findAll() {
		List<TblPagina> sect = new ArrayList<TblPagina>();
		Query query = utilFacade.getEm().createQuery(
				"Select sect from TblPagina as sect");
		sect = query.getResultList();
		return sect;
	}

	public List<FkRolPagina> findAllRolPagina(Integer idRol) {
		List<FkRolPagina> roles = new ArrayList<FkRolPagina>();
		Query query = utilFacade
				.getEm()
				.createQuery(
						"Select roles from FkRolPagina as roles where roles.id.idValuenivel=:idRol");
		query.setParameter("idRol", idRol);
		roles = query.getResultList();
		return roles;
	}

}
