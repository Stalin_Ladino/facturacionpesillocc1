package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.util.UtilConexion;

@Stateless
public class secComDTOImpl implements secComDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(secComDTO secComDTO) {
		utilFacade.getEm().persist(secComDTO);
	}

	@Override
	public void edit(secComDTO secComDTO) {
		utilFacade.getEm().merge(secComDTO);
		
	}

	@Override
	public void remove(secComDTO secComDTO) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(secComDTO));
		
	}
	
//	
	public secComDTO findById(Integer id){
		secComDTO sec= new secComDTO();
		Query query=utilFacade.getEm().createQuery("Select sec from secComDTO as sec where idSec=:id ");
		query.setParameter("id", id);
		sec= (secComDTO) query.getSingleResult();
		return sec;
	}

	public List<secComDTO> findAll(){
		List<secComDTO> sect= new ArrayList<secComDTO>();
		Query query=utilFacade.getEm().createQuery("Select sect from secComDTO as sect");
		sect= query.getResultList();
		return sect;
	}
	
	public List<secComDTO> findAllBySector(Integer idSect){
		List<secComDTO> sect= new ArrayList<secComDTO>();
		Query query=utilFacade.getEm().createQuery("Select sect from secComDTO as sect"
												 + " where sect.idSec=:idSect");
		
		query.setParameter("idSect", idSect);
		sect= query.getResultList();
		return sect;
	}
	
	public List<secComDTO> buscarParametros(){
		List<secComDTO> sect= new ArrayList<secComDTO>();
		Query query=utilFacade.getEm().createQuery("Select param from secComDTO as sect,TblParametro as param"
												 + " where sect.idSec=param.idSec");
		sect= query.getResultList();
		return sect;
	}
	
	public List<secComDTO> buscarSectoresUser(String cedula){
		List<secComDTO> sect= new ArrayList<secComDTO>();
//		Query query=utilFacade.getEm().createQuery("Select sect from secComDTO as sect,"
//												 + " usuarioDTO as user"
//												 + " where sect.idSec=user.idSec"
//												 + " and user.cedUsu=:cedula");
		
		Query query=utilFacade.getEm().createQuery("Select sect from TblSectorUsuario as sect,"
				 + " usuarioDTO as user"
				 + " where sect.idUsu=user.idUsu"
				 + " and user.cedUsu=:cedula");
		
		query.setParameter("cedula", cedula);
		sect= query.getResultList();
		return sect;
	}
		
}
