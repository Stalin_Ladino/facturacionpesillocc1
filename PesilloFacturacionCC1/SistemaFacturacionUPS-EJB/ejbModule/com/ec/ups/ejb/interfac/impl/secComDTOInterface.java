package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.secComDTO;

@Local
public interface secComDTOInterface {
	
	void create(secComDTO secComDTO);

	void edit(secComDTO secComDTO);

	void remove(secComDTO secComDTO);

	public secComDTO findById(Integer id);
	
	public List<secComDTO> findAll();
	
	public List<secComDTO> buscarParametros();
	
	public List<secComDTO> buscarSectoresUser(String cedula);
	
	public List<secComDTO> findAllBySector(Integer idSect);
	
}
