package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.ec.ups.util.UtilConexion;

@Stateless
public class usuarioDTOImpl implements usuarioDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(usuarioDTO usuarioDTO) {
		
//		usuarioDTO.getLoginDTO().setIdValuenivel(usuarioDTO.getLoginDTO().getTblRol().getIdValuenivel());
//		usuarioDTO.getLoginDTO().setTblRol(null);
//		utilFacade.getEm().persist(usuarioDTO.getLoginDTO());
//		usuarioDTO.setIdLogin(usuarioDTO.getLoginDTO().getIdLogin());
		usuarioDTO.setIdUsu(null);
		usuarioDTO.setLoginDTO(null);
		//cambio 
		usuarioDTO.setTblSectorUsuario(null);
		usuarioDTO.setTblUsuarioMulta(null);
		usuarioDTO.setTblMedidors(null);
		utilFacade.getEm().persist(usuarioDTO);
	}

	@Override
	public void edit(usuarioDTO usuarioDTO) {
//		usuarioDTO.getLoginDTO().setIdValuenivel(usuarioDTO.getLoginDTO().getTblRol().getIdValuenivel());
		usuarioDTO.setTblSectorUsuario(null);
		usuarioDTO.setLoginDTO(null);
		usuarioDTO.setTblMedidors(null);
		usuarioDTO.setTblSectorUsuario(null);
		usuarioDTO.setTblUsuarioMulta(null);
		
		
//		usuarioDTO.getLoginDTO().setTblRol(null);
		utilFacade.getEm().merge(usuarioDTO);
		
	}
 
	@Override
	public void remove(usuarioDTO usuarioDTO) {
//		usuarioDTO.setRolDTO(null);
		usuarioDTO.setTblSectorUsuario(null);
//		usuarioDTO.getLoginDTO().setTblRol(null);
		utilFacade.getEm().remove(utilFacade.getEm().merge(usuarioDTO));
		
	}
	
//	
	public usuarioDTO findById(Integer id){
		usuarioDTO user= new usuarioDTO();
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user where idUsu=:id ");
		query.setParameter("id", id);
		user= (usuarioDTO) query.getSingleResult();
		return user;
	}
	
	@Override
	public void eliminarPorSector(Integer codsec) {
		
		Query query=utilFacade.getEm().createQuery("Delete from usuarioDTO as user where user.idSec=:codsec");
		query.setParameter("codsec", codsec);
		query.executeUpdate();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<usuarioDTO> findAll(){
		List<usuarioDTO> listUser= new ArrayList<usuarioDTO>();
		
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user");
		listUser= query.getResultList();
		return listUser;
	}
	

	@SuppressWarnings("unchecked")
	public List<usuarioDTO> findUserAll(){
		List<usuarioDTO> listUser= new ArrayList<usuarioDTO>();
		
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user,"
				+ " loginDTO as login,rolDTO as rol,TblSectorUsuario as tblSecUser"
				+ " where user.idLogin = login.idLogin"
				+ " and tblSecUser.idLogin = login.idLogin"
				+ " and tblSecUser.idValuenivel = rol.idValuenivel"
				+ " and tblSecUser.idUsu = user.idUsu"
				+ " ORDER BY user.idLogin"
				);
		listUser= query.getResultList();
		return listUser;
	}

	public usuarioDTO findByIdLogin(Integer id){
		usuarioDTO user= new usuarioDTO();
		try {
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user,"
				+ " loginDTO as login"
				+ " where user.idLogin=:id "
				+ " and user.idLogin = login.idLogin");
		query.setParameter("id", id);
		user= (usuarioDTO) query.getSingleResult();
		
		} catch (Exception e) {
			
			loginDTO login= new loginDTO();
			
			Query query=utilFacade.getEm().createQuery("Select login from loginDTO as login"
					+ " where login.idLogin=:id ");
			query.setParameter("id", id);
			login= (loginDTO) query.getSingleResult();
			user.setLoginDTO(login);
		}
		return user;
	}

	@Override
	public usuarioDTO findByCedula(String cedula) {
		usuarioDTO user= new usuarioDTO();
		try{
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user"
				+ " where user.cedUsu=:cedula ");
		
		query.setParameter("cedula", cedula);
		query.setMaxResults(ConstantesUtil.NUMERO_UNO_INT);
		user= (usuarioDTO) query.getSingleResult();
		} catch(NoResultException e){
			user=null;
		}
		return user;
	}
	
	@Override
	public List<usuarioDTO> buscarByCedula(String cedula) {
		List<usuarioDTO> user= new ArrayList<usuarioDTO>();
		try{
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user,"
				+ " TblSectorUsuario as secUser"
				+ " where user.cedUsu=:cedula "
				+ " and secUser.idUsu=user.idUsu");
		
		query.setParameter("cedula", cedula);
		user= query.getResultList();
		} catch(NoResultException e){
		user=null;
		}
		return user;
	}
	
	@Override
	public List<usuarioDTO> buscarByCedulaSector(String cedula,Integer idSector) {
		List<usuarioDTO> user= new ArrayList<usuarioDTO>();
		try{
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user,"
				+ " TblSectorUsuario as secUser"
				+ " where user.cedUsu=:cedula "
				+ " and user.idUsu=secUser.idUsu"
				+ " and secUser.idSec=:idSector");
		
		query.setParameter("cedula", cedula);
		query.setParameter("idSector", idSector);
		user= query.getResultList();
		} catch(NoResultException e){
		user=null;
		}
		return user;
	}
	@Override
	public List<TblSectorUsuario> buscarByLoginSector(Integer idLogin) {
		List<TblSectorUsuario> user= new ArrayList<TblSectorUsuario>();
		try{
		Query query=utilFacade.getEm().createQuery("Select tblSector from usuarioDTO as user,"
				+ " secComDTO as secCom, TblSectorUsuario as tblSector"
				+ " where user.idLogin=:idLogin"
				+ " and tblSector.idUsu=user.idUsu"
				+ " and tblSector.idSec=secCom.idSec");
		
		query.setParameter("idLogin", idLogin);
		user= query.getResultList();
		} catch(NoResultException e){
		user=null;
		}
		return user;
	}
	@Override
	public List<usuarioDTO> buscarBySector(Integer idSector) {
		List<usuarioDTO> user= new ArrayList<usuarioDTO>();
		try{
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user,"
				+ " TblSectorUsuario as secUser"
				+ " where user.idUsu=secUser.idUsu"
				+ " and secUser.idSec=:idSec");
		
		query.setParameter("idSec", idSector);
		user= query.getResultList();
		} catch(NoResultException e){
		user=null;
		}
		return user;
	}	
	
	
	@Override
	public List<usuarioDTO> buscarByLoginList(Integer idLogin) {
		List<usuarioDTO> user= new ArrayList<usuarioDTO>();
		try{
		Query query=utilFacade.getEm().createQuery("Select user from usuarioDTO as user"
				+ " where user.idLogin=:idLogin ");
		
		query.setParameter("idLogin", idLogin);
		user= query.getResultList();
		} catch(NoResultException e){
		user=null;
		}
		return user;
	}	
	
}
