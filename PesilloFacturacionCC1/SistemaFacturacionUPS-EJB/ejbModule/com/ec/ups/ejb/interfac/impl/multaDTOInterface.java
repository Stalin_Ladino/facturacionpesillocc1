package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblMulta;


@Local
public interface multaDTOInterface {
	
	void create(TblMulta TblMulta);

	void edit(TblMulta TblMulta);

	void remove(TblMulta TblMulta);

	public TblMulta findById(Integer id);
	
	public List<TblMulta> buscarBySector(Integer idSec);
	
	public List<TblMulta> findByAll();
	
}
