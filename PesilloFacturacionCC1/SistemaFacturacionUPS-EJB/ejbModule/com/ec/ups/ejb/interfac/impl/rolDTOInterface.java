package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.rolDTO;

@Local
public interface rolDTOInterface {
	
	void create(rolDTO rolDTO);

	void edit(rolDTO rolDTO);

	void remove(rolDTO rolDTO);

	public rolDTO findById(Integer id);
	
	public List<rolDTO> findAll();
	
	public Integer buscarUltimoId();
	
	public List<rolDTO> findByRolAdministrator();
	
}
