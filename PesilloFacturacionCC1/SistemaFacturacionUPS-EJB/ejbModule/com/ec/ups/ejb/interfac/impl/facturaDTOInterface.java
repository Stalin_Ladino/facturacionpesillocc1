package com.ec.ups.ejb.interfac.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.ResultadoFactura;
import com.ec.ups.jpa.entidades.TblFactura;

@Local
public interface facturaDTOInterface {
	
	void create(TblFactura TblFactura);

	void edit(TblFactura TblFactura);

	void remove(TblFactura TblFactura);

	public List<ResultadoFactura> cargarFactura(String cedula,
			Integer idSector,
			Date fechaIni,
			Date fechaFin,
			Integer idServicio );
	
	
}
