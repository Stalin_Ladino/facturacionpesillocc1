package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblRegional;


@Local
public interface regionalDTOInterface {
	
	void create(TblRegional TblRegional);

	void edit(TblRegional TblRegional);

	void remove(TblRegional TblRegional);

	public TblRegional findById(Integer id);
	
	List<TblRegional> findAll();
	
//	List<TblRegional> findUserAll();
	
	
}
