package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.rolDTO;
import com.ec.ups.util.UtilConexion;

@Stateless
public class rolDTOImpl implements rolDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(rolDTO rolDTO) {
		Integer idValue=this.buscarUltimoId()+1;
		rolDTO.setIdNivelacceso(idValue);
		rolDTO.setIdValuenivel(idValue);
		utilFacade.getEm().persist(rolDTO);
	}

	@Override
	public void edit(rolDTO rolDTO) {
		utilFacade.getEm().merge(rolDTO);
		
	}

	@Override
	public void remove(rolDTO rolDTO) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(rolDTO));
		
	}
	
//	
	public rolDTO findById(Integer id){
		rolDTO rol= new rolDTO();
		Query query=utilFacade.getEm().createQuery("Select rol from rolDTO as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (rolDTO) query.getSingleResult();
		return rol;
	}

	public List<rolDTO> findAll(){
		List<rolDTO> rol= new ArrayList<rolDTO>();
		Query query=utilFacade.getEm().createQuery("Select rol from rolDTO as rol");
		rol= query.getResultList();
		return rol;
	}
	
	public List<rolDTO> findByRolAdministrator(){
		List<rolDTO> rol= new ArrayList<rolDTO>();
		Query query=utilFacade.getEm().createQuery("Select rol from rolDTO as rol where rol.idValuenivel!=1");
		rol= query.getResultList();
		return rol;
	}
	
	public Integer buscarUltimoId(){
		String sql="select max(tbl_rol.id_nivelacceso) from tbl_rol";
		Query query=utilFacade.getEm().createNativeQuery(sql);
		Integer idValue= (Integer)query.getSingleResult();
		return idValue;
		 
	}
	

	
}
