package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.FkRolPagina;
import com.ec.ups.jpa.entidades.TblPagina;

@Local
public interface paginaDTOInterface {
	
	void create(TblPagina TblPagina);

	void edit(TblPagina TblPagina);

	void remove(TblPagina TblPagina);

	public List<TblPagina>findAllByLevel(Integer id);
	
	public List<TblPagina> findAll();
	
	public TblPagina buscarLogin(String nombreUs, String pass);
	
	public List<FkRolPagina> findAllRolPagina(Integer idRol);
	
	void createRolPagina(FkRolPagina fkRolPagina);
	
	void removeRolPagina(FkRolPagina fkRolPagina);
	
}
