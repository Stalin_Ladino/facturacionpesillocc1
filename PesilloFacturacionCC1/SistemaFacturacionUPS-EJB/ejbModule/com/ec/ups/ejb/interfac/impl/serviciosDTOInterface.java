package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblServicio;

@Local
public interface serviciosDTOInterface {
	
	void create(TblServicio TblServicio);

	void edit(TblServicio TblServicio);

	void remove(TblServicio TblServicio);

	public TblServicio findById(Integer id);
	
	public List<TblServicio> findAll();
	
}
