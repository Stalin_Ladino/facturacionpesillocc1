package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * The persistent class for the tbl_multa database table.
 * 
 */
@Entity
@Table(name = "tbl_multa")
@NamedQuery(name = "TblMulta.findAll", query = "SELECT t FROM TblMulta t")
public class TblMulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_multa")
	private Integer idMulta;

	@Column(name = "det_multa")
	private String detMulta;

	@Column(name = "id_sec")
	private Integer idSec;

	@Column(name = "val_multa")
	private Double valMulta;

	// bi-directional many-to-one association to TblSecCom
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_sec", insertable = false, updatable = false)
	private secComDTO secComDTO;

	// bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy = "tblMulta", orphanRemoval = true)
	private List<TblUsuarioMulta> tblUsuarioMulta;

	public TblMulta() {
	}

	public Integer getIdMulta() {
		return this.idMulta;
	}

	public void setIdMulta(Integer idMulta) {
		this.idMulta = idMulta;
	}

	public String getDetMulta() {
		return this.detMulta;
	}

	public void setDetMulta(String detMulta) {
		this.detMulta = detMulta;
	}

	public Integer getIdSec() {
		return this.idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

	public Double getValMulta() {
		return this.valMulta;
	}

	public void setValMulta(Double valMulta) {
		this.valMulta = valMulta;
	}

	public secComDTO getSecComDTO() {
		return secComDTO;
	}

	public void setSecComDTO(secComDTO secComDTO) {
		this.secComDTO = secComDTO;
	}

	public List<TblUsuarioMulta> getTblUsuarioMulta() {
		return tblUsuarioMulta;
	}

	public void setTblUsuarioMulta(List<TblUsuarioMulta> tblUsuarioMulta) {
		this.tblUsuarioMulta = tblUsuarioMulta;
	}

}