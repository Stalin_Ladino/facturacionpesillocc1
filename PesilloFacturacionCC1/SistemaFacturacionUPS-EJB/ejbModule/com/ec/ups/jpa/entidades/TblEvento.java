package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the tbl_eventos database table.
 * 
 */
@Entity
@Table(name="tbl_eventos")
@NamedQuery(name="TblEvento.findAll", query="SELECT t FROM TblEvento t")
public class TblEvento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_evento")
	private Integer idEvento;

	@Column(name="descripcion_event")
	private String descripcionEvent;

	@Column(name="titulo_evento")
	private String tituloEvento;

	public TblEvento() {
	}

	public Integer getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}

	public String getDescripcionEvent() {
		return this.descripcionEvent;
	}

	public void setDescripcionEvent(String descripcionEvent) {
		this.descripcionEvent = descripcionEvent;
	}

	public String getTituloEvento() {
		return this.tituloEvento;
	}

	public void setTituloEvento(String tituloEvento) {
		this.tituloEvento = tituloEvento;
	}

}