package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the tbl_sector_usuario database table.
 * 
 */
@Entity
@Table(name="tbl_sector_usuario")
@NamedQuery(name="TblSectorUsuario.findAll", query="SELECT t FROM TblSectorUsuario t")
public class TblSectorUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_sec_usu")
	private Integer idSecUsu;

	@Column(name="id_login")
	private Integer idLogin;

	@Column(name="id_sec")
	private Integer idSec;

	@Column(name="id_usu")
	private Integer idUsu;

	@Column(name="id_valuenivel")
	private Integer idValuenivel;
	
	@ManyToOne
	@JoinColumn(name="id_usu",updatable=false,insertable=false)
	private usuarioDTO tblUsuario;
	
	@ManyToOne
	@JoinColumn(name="id_sec",updatable=false,insertable=false)
	private secComDTO secComDTO;
	
	@ManyToOne
	@JoinColumn(name="id_login",updatable=false,insertable=false)
	private loginDTO loginDTO;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_valuenivel", insertable=false ,updatable=false)
	private rolDTO tblRol;
	
	public TblSectorUsuario() {
	}

	public Integer getIdSecUsu() {
		return this.idSecUsu;
	}

	public void setIdSecUsu(Integer idSecUsu) {
		this.idSecUsu = idSecUsu;
	}

	public Integer getIdLogin() {
		return this.idLogin;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public Integer getIdSec() {
		return this.idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

	public Integer getIdUsu() {
		return this.idUsu;
	}

	public void setIdUsu(Integer idUsu) {
		this.idUsu = idUsu;
	}

	public Integer getIdValuenivel() {
		return this.idValuenivel;
	}

	public void setIdValuenivel(Integer idValuenivel) {
		this.idValuenivel = idValuenivel;
	}

	public usuarioDTO getTblUsuario() {
		return tblUsuario;
	}

	public void setTblUsuario(usuarioDTO tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public secComDTO getSecComDTO() {
		return secComDTO;
	}

	public void setSecComDTO(secComDTO secComDTO) {
		this.secComDTO = secComDTO;
	}

	public loginDTO getLoginDTO() {
		return loginDTO;
	}

	public void setLoginDTO(loginDTO loginDTO) {
		this.loginDTO = loginDTO;
	}

	public rolDTO getTblRol() {
		return tblRol;
	}

	public void setTblRol(rolDTO tblRol) {
		this.tblRol = tblRol;
	}

}