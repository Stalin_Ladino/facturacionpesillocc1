package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the tbl_login database table.
 * 
 */
@Entity
@Table(name="tbl_login")
@NamedQuery(name="loginDTO.findAll", query="SELECT t FROM loginDTO t")
public class loginDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_login")
	private Integer idLogin;

	@Column(name="nom_user")
	private String nomUser;

	@Column(name="pass_user")
	private String passUser;
	
	@Transient
	private Integer idValuenivel;
//
//	//bi-directional many-to-one association to TblRol
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name="id_valuenivel", insertable=false ,updatable=false)
//	private rolDTO tblRol;

	//bi-directional many-to-one association to TblUsuario
	@OneToMany(mappedBy="loginDTO", orphanRemoval=true)
	private List<usuarioDTO> tblUsuarios;
	
	@OneToMany(mappedBy="loginDTO", orphanRemoval=true)
	private List<TblSectorUsuario> tblSectorUsuario;

	public loginDTO() {
	}

	public Integer getIdLogin() {
		return this.idLogin;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public String getNomUser() {
		return this.nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getPassUser() {
		return this.passUser;
	}

	public void setPassUser(String passUser) {
		this.passUser = passUser;
	}

//	public rolDTO getTblRol() {
//		return this.tblRol;
//	}
//
//	public void setTblRol(rolDTO tblRol) {
//		this.tblRol = tblRol;
//	}

	public List<usuarioDTO> getTblUsuarios() {
		return this.tblUsuarios;
	}

	public void setTblUsuarios(List<usuarioDTO> tblUsuarios) {
		this.tblUsuarios = tblUsuarios;
	}

	public usuarioDTO addTblUsuario(usuarioDTO tblUsuario) {
		getTblUsuarios().add(tblUsuario);
		tblUsuario.setLoginDTO(this);

		return tblUsuario;
	}

	public usuarioDTO removeTblUsuario(usuarioDTO tblUsuario) {
		getTblUsuarios().remove(tblUsuario);
		tblUsuario.setLoginDTO(null);

		return tblUsuario;
	}

	public List<TblSectorUsuario> getTblSectorUsuario() {
		return tblSectorUsuario;
	}

	public void setTblSectorUsuario(List<TblSectorUsuario> tblSectorUsuario) {
		this.tblSectorUsuario = tblSectorUsuario;
	}

	public Integer getIdValuenivel() {
		return idValuenivel;
	}

	public void setIdValuenivel(Integer idValuenivel) {
		this.idValuenivel = idValuenivel;
	}

//	public Integer getIdValuenivel() {
//		return idValuenivel;
//	}
//
//	public void setIdValuenivel(Integer idValuenivel) {
//		this.idValuenivel = idValuenivel;
//	}

	
}