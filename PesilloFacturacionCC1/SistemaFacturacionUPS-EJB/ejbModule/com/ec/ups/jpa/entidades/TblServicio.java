package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the tbl_servicio database table.
 * 
 */
@Entity
@Table(name="tbl_servicio")
@NamedQuery(name="TblServicio.findAll", query="SELECT t FROM TblServicio t")
public class TblServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_serv")
	private Integer idServ;

	@Column(name="nom_serv")
	private String nomServ;

	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblServicio")
	private List<TblMedidor> tblMedidors;
	
	@OneToMany(mappedBy="tblServicio")
	private List<TblParametro> tblParametro;

	public TblServicio() {
	}

	public Integer getIdServ() {
		return this.idServ;
	}

	public void setIdServ(Integer idServ) {
		this.idServ = idServ;
	}

	public String getNomServ() {
		return this.nomServ;
	}

	public void setNomServ(String nomServ) {
		this.nomServ = nomServ;
	}

	public List<TblMedidor> getTblMedidors() {
		return this.tblMedidors;
	}

	public void setTblMedidors(List<TblMedidor> tblMedidors) {
		this.tblMedidors = tblMedidors;
	}

	public TblMedidor addTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().add(tblMedidor);
		tblMedidor.setTblServicio(this);

		return tblMedidor;
	}

	public TblMedidor removeTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().remove(tblMedidor);
		tblMedidor.setTblServicio(null);

		return tblMedidor;
	}

}