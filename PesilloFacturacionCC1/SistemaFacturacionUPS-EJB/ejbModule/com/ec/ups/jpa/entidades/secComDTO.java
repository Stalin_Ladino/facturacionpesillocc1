package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tbl_sec_com database table.
 * 
 */
@Entity
@Table(name="tbl_sec_com")
@NamedQuery(name="secComDTO.findAll", query="SELECT t FROM secComDTO t")
public class secComDTO implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_sec")
	private Integer idSec;

	@Column(name="cod_sec")
	private String codSec;

	@Column(name="hab_sec")
	private Long habSec;

	@Column(name="nom_sec")
	private String nomSec;

	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblSecCom")
	private List<TblMedidor> tblMedidors;

	@Column(name="id_reg")
	private Integer idReg;
	
	@ManyToOne
	@JoinColumn(name="id_reg",insertable=false,updatable=false)
	private TblRegional tblRegional;
	
	@OneToMany(mappedBy="secComDTO")
	private List<TblParametro> tblParametro;
	//bi-directional many-to-one association to TblUsuario
	
//	@OneToMany(mappedBy="secComDTO",orphanRemoval=true)
//	private List<usuarioDTO> tblUsuarios;

	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="secComDTO",orphanRemoval=true)
	private List<TblMulta> tblMulta;
	
	// bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy = "secComDTO", orphanRemoval = true)
	private List<TblSectorUsuario> TblSectorUsuario;
	
	public secComDTO() {
	}

	public Integer getIdSec() {
		return this.idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

	public String getCodSec() {
		return this.codSec;
	}

	public void setCodSec(String codSec) {
		this.codSec = codSec;
	}

	public Long getHabSec() {
		return this.habSec;
	}

	public void setHabSec(Long habSec) {
		this.habSec = habSec;
	}

	public String getNomSec() {
		return this.nomSec;
	}

	public void setNomSec(String nomSec) {
		this.nomSec = nomSec;
	}

	public List<TblMedidor> getTblMedidors() {
		return this.tblMedidors;
	}

	public void setTblMedidors(List<TblMedidor> tblMedidors) {
		this.tblMedidors = tblMedidors;
	}

	public TblMedidor addTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().add(tblMedidor);
		tblMedidor.setTblSecCom(this);

		return tblMedidor;
	}

	public TblMedidor removeTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().remove(tblMedidor);
		tblMedidor.setTblSecCom(null);

		return tblMedidor;
	}

	public TblRegional getTblRegional() {
		return this.tblRegional;
	}

	public void setTblRegional(TblRegional tblRegional) {
		this.tblRegional = tblRegional;
	}

//	public List<usuarioDTO> getTblUsuarios() {
//		return this.tblUsuarios;
//	}
//
//	public void setTblUsuarios(List<usuarioDTO> tblUsuarios) {
//		this.tblUsuarios = tblUsuarios;
//	}
	

//	public usuarioDTO addTblUsuario(usuarioDTO tblUsuario) {
//		getTblUsuarios().add(tblUsuario);
//		tblUsuario.setSecComDTO(this);
//
//		return tblUsuario;
//	}
//
//	public usuarioDTO removeTblUsuario(usuarioDTO tblUsuario) {
//		getTblUsuarios().remove(tblUsuario);
//		tblUsuario.setSecComDTO(null);
//
//		return tblUsuario;
//	}
	
	

	public Integer getIdReg() {
		return idReg;
	}

	public List<TblSectorUsuario> getTblSectorUsuario() {
		return TblSectorUsuario;
	}

	public void setTblSectorUsuario(List<TblSectorUsuario> tblSectorUsuario) {
		TblSectorUsuario = tblSectorUsuario;
	}

	public void setIdReg(Integer idReg) {
		this.idReg = idReg;
	}

	public List<TblParametro> getTblParametro() {
		return tblParametro;
	}

	public void setTblParametro(List<TblParametro> tblParametro) {
		this.tblParametro = tblParametro;
	}

	public List<TblMulta> getTblMulta() {
		return tblMulta;
	}

	public void setTblMulta(List<TblMulta> tblMulta) {
		this.tblMulta = tblMulta;
	}

}