package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the fk_rol_paginas database table.
 * 
 */
@Embeddable
public class FkRolPaginaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_valuenivel", insertable=false, updatable=false)
	private Integer idValuenivel;

	@Column(name="id_pag", insertable=false, updatable=false)
	private Integer idPag;

	public FkRolPaginaPK() {
	}
	public Integer getIdValuenivel() {
		return this.idValuenivel;
	}
	public void setIdValuenivel(Integer idValuenivel) {
		this.idValuenivel = idValuenivel;
	}
	public Integer getIdPag() {
		return this.idPag;
	}
	public void setIdPag(Integer idPag) {
		this.idPag = idPag;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FkRolPaginaPK)) {
			return false;
		}
		FkRolPaginaPK castOther = (FkRolPaginaPK)other;
		return 
			this.idValuenivel.equals(castOther.idValuenivel)
			&& this.idPag.equals(castOther.idPag);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idValuenivel.hashCode();
		hash = hash * prime + this.idPag.hashCode();
		
		return hash;
	}
}