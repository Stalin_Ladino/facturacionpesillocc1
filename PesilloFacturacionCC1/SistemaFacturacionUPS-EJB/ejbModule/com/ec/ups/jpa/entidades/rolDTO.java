package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the tbl_rol database table.
 * 
 */
@Entity
@Table(name="tbl_rol")
@NamedQuery(name="rolDTO.findAll", query="SELECT t FROM rolDTO t")
public class rolDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_valuenivel")
	private Integer idValuenivel;

	private String descripcion;

	@Column(name="id_nivelacceso")
	private Integer idNivelacceso;

	//bi-directional many-to-many association to TblPagina
	@ManyToMany
	@JoinTable(
		name="fk_rol_paginas"
		, joinColumns={
			@JoinColumn(name="id_valuenivel")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_pag")
			}
		)
	private List<TblPagina> tblPaginas;

	//bi-directional many-to-one association to TblLogin
//	@OneToMany(mappedBy="tblRol")
//	private List<loginDTO> tblLogins;
	
	@OneToMany(mappedBy="tblRol")
	private List<TblSectorUsuario> tblSectorUsuario;
	

	public rolDTO() {
	}

	public Integer getIdValuenivel() {
		return this.idValuenivel;
	}

	public void setIdValuenivel(Integer idValuenivel) {
		this.idValuenivel = idValuenivel;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getIdNivelacceso() {
		return this.idNivelacceso;
	}

	public void setIdNivelacceso(Integer idNivelacceso) {
		this.idNivelacceso = idNivelacceso;
	}

	public List<TblPagina> getTblPaginas() {
		return this.tblPaginas;
	}

	public void setTblPaginas(List<TblPagina> tblPaginas) {
		this.tblPaginas = tblPaginas;
	}

	public List<TblSectorUsuario> getTblSectorUsuario() {
		return tblSectorUsuario;
	}

	public void setTblSectorUsuario(List<TblSectorUsuario> tblSectorUsuario) {
		this.tblSectorUsuario = tblSectorUsuario;
	}
	
	

//	public List<loginDTO> getTblLogins() {
//		return this.tblLogins;
//	}
//
//	public void setTblLogins(List<loginDTO> tblLogins) {
//		this.tblLogins = tblLogins;
//	}
//
//	public loginDTO addTblLogin(loginDTO tblLogin) {
//		getTblLogins().add(tblLogin);
//		tblLogin.setTblRol(this);
//
//		return tblLogin;
//	}
//
//	public loginDTO removeTblLogin(loginDTO tblLogin) {
//		getTblLogins().remove(tblLogin);
//		tblLogin.setTblRol(null);
//
//		return tblLogin;
//	}

}