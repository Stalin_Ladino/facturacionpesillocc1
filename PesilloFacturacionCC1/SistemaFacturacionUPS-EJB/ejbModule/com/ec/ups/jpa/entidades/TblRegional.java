package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_regional database table.
 * 
 */
@Entity
@Table(name="tbl_regional")
@NamedQuery(name="TblRegional.findAll", query="SELECT t FROM TblRegional t")
public class TblRegional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_reg")
	private Integer idReg;

	@Column(name="cod_reg")
	private String codReg;

	@Column(name="desc_reg")
	private String descReg;

	@Column(name="nom_reg")
	private String nomReg;

	@Column(name="rep_reg")
	private String repReg;

	@Column(name="ubi_reg")
	private String ubiReg;

	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblRegional",orphanRemoval=true)
	private List<TblMedidor> tblMedidors;

	//bi-directional many-to-one association to TblSecCom
	@OneToMany(mappedBy="tblRegional",orphanRemoval=true)
	private List<secComDTO> tblSecComs;

	public TblRegional() {
	}

	public Integer getIdReg() {
		return this.idReg;
	}

	public void setIdReg(Integer idReg) {
		this.idReg = idReg;
	}

	public String getCodReg() {
		return this.codReg;
	}

	public void setCodReg(String codReg) {
		this.codReg = codReg;
	}

	public String getDescReg() {
		return this.descReg;
	}

	public void setDescReg(String descReg) {
		this.descReg = descReg;
	}

	public String getNomReg() {
		return this.nomReg;
	}

	public void setNomReg(String nomReg) {
		this.nomReg = nomReg;
	}

	public String getRepReg() {
		return this.repReg;
	}

	public void setRepReg(String repReg) {
		this.repReg = repReg;
	}

	public String getUbiReg() {
		return this.ubiReg;
	}

	public void setUbiReg(String ubiReg) {
		this.ubiReg = ubiReg;
	}

	public List<TblMedidor> getTblMedidors() {
		return this.tblMedidors;
	}

	public void setTblMedidors(List<TblMedidor> tblMedidors) {
		this.tblMedidors = tblMedidors;
	}

	public TblMedidor addTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().add(tblMedidor);
		tblMedidor.setTblRegional(this);

		return tblMedidor;
	}

	public TblMedidor removeTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().remove(tblMedidor);
		tblMedidor.setTblRegional(null);

		return tblMedidor;
	}

	public List<secComDTO> getTblSecComs() {
		return this.tblSecComs;
	}

	public void setTblSecComs(List<secComDTO> tblSecComs) {
		this.tblSecComs = tblSecComs;
	}

	public secComDTO addTblSecCom(secComDTO tblSecCom) {
		getTblSecComs().add(tblSecCom);
		tblSecCom.setTblRegional(this);

		return tblSecCom;
	}

	public secComDTO removeTblSecCom(secComDTO tblSecCom) {
		getTblSecComs().remove(tblSecCom);
		tblSecCom.setTblRegional(null);

		return tblSecCom;
	}

}