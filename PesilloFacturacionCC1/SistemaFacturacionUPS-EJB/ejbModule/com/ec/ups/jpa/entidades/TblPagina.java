package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_paginas database table.
 * 
 */
@Entity
@Table(name="tbl_paginas")
@NamedQuery(name="TblPagina.findAll", query="SELECT t FROM TblPagina t")
public class TblPagina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_pag")
	private Integer idPag;

	@Column(name="descripcion_pag_")
	private String descripcionPag;

	@Column(name="path_pag_")
	private String pathPag;

	//bi-directional many-to-many association to TblRol
	@ManyToMany(mappedBy="tblPaginas")
	private List<rolDTO> tblRols;
	
	@Transient
	private Boolean selectPagina;

	public TblPagina() {
	}

	public Integer getIdPag() {
		return this.idPag;
	}

	public void setIdPag(Integer idPag) {
		this.idPag = idPag;
	}

	public String getDescripcionPag() {
		return this.descripcionPag;
	}

	public void setDescripcionPag(String descripcionPag) {
		this.descripcionPag = descripcionPag;
	}

	public String getPathPag() {
		return this.pathPag;
	}

	public void setPathPag(String pathPag) {
		this.pathPag = pathPag;
	}

	public List<rolDTO> getTblRols() {
		return this.tblRols;
	}

	public void setTblRols(List<rolDTO> tblRols) {
		this.tblRols = tblRols;
	}

	public Boolean getSelectPagina() {
		return selectPagina;
	}

	public void setSelectPagina(Boolean selectPagina) {
		this.selectPagina = selectPagina;
	}
	
	

}