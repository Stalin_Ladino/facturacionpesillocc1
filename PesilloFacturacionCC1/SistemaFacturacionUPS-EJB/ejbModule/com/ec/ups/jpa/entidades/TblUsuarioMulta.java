package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the tbl_usuario_multa database table.
 * 
 */
@Entity
@Table(name="tbl_usuario_multa")
@NamedQuery(name="TblUsuarioMulta.findAll", query="SELECT t FROM TblUsuarioMulta t")
public class TblUsuarioMulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_usu_mul")
	private Integer idUsuMul;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_reg")
	private Date fechaReg;

	@Column(name="id_multa")
	private Integer idMulta;

	@Column(name="id_usu")
	private Integer idUsu;
	
	@ManyToOne
	@JoinColumn(name="id_usu",updatable=false,insertable=false)
	private usuarioDTO tblUsuario;
	
	@ManyToOne
	@JoinColumn(name="id_multa",updatable=false,insertable=false)
	private TblMulta tblMulta;

	public TblUsuarioMulta() {
	}

	public Integer getIdUsuMul() {
		return this.idUsuMul;
	}

	public void setIdUsuMul(Integer idUsuMul) {
		this.idUsuMul = idUsuMul;
	}

	public Date getFechaReg() {
		return this.fechaReg;
	}

	public void setFechaReg(Date fechaReg) {
		this.fechaReg = fechaReg;
	}

	public Integer getIdMulta() {
		return this.idMulta;
	}

	public void setIdMulta(Integer idMulta) {
		this.idMulta = idMulta;
	}

	public Integer getIdUsu() {
		return this.idUsu;
	}

	public void setIdUsu(Integer idUsu) {
		this.idUsu = idUsu;
	}

	public usuarioDTO getTblUsuario() {
		return tblUsuario;
	}

	public void setTblUsuario(usuarioDTO tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public TblMulta getTblMulta() {
		return tblMulta;
	}

	public void setTblMulta(TblMulta tblMulta) {
		this.tblMulta = tblMulta;
	}
	
	

}