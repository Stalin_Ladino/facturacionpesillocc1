--------------------------------------------------
--FUNCIONES Y TYPE PARA CARGAR DEPOSITO
--------------------------------------------------

--Creacion de type de variable para almacenar datos de carga depositos
create or replace TYPE CARGA_DEPOSITO_VAR AS OBJECT
    ( NOMFARM           VARCHAR2(100)
    , FECHA             DATE
    , DOCUMENTO         VARCHAR2(100)
    , TIPODEP           VARCHAR2(100)
    , CUENTA            VARCHAR2(100)
    , VALOR               NUMBER(15,5)
    , SALDO               NUMBER(15,5)
    , NUMFAR               NUMBER(15,5)
    ) ;

--Creacion de tabla de referencia de typo
create or replace type myTableType as table of CARGA_DEPOSITO_VAR

--Creacion de funci�n para retornar valores de cara por deposito
create or replace FUNCTION cargaDeposito (tipoDep NUMBER,fechaDesde STRING, fechaHasta STRING)
RETURN myTableType
is
l_data myTableType;
    NOMFARM              VARCHAR2(100);
    FECHA                DATE;
    DOCUMENTO            VARCHAR2(100);
    VALOR                NUMBER(15,5);
    SALDO                NUMBER(15,5);
    FARMACIAID           NUMBER(15,5);
    NUMFAR               NUMBER(15,5);
    cont                 int;
    vn_cuenta            NUMBER:=0;
    numero_cuenta        VARCHAR2(100);
    tipo_dep             VARCHAR2(100);
   

    CURSOR C_PERF IS 
SELECT ADF.NOMBRE,CVD.DOCUMENTO,CVD.FARMACIA,CVD.FECHA,CVD.VALOR,CVD.SALDO, ADF.CODIGO
 FROM IN_VENCIMIENTO_DEPOSITOS CVD,AD_FARMACIAS ADF
                          where (CVD.ACTIVA = 'S' and CVD.SALDO <> 0)  and  
										      exists (select  y.CUENTA from in_registros_dep_far y  where y.farmacia = cvd.farmacia 
										      and y.numero_deposito = cvd.documento 
                          and nvl(cp_var3, 'N') <> 'C' 
                          and (cuenta = NULL or NULL is null) 
										      and y.tipo_deposito in (1, 6, 8))
                          and CVD.FECHA >= to_date('01/06/2014', 'dd/mm/yyyy')
                          and (CVD.FECHA >= to_date(fechaDesde, 'dd/mm/yyyy HH24:MI:SS') or fechaDesde is null) 
                          and (CVD.FECHA <= to_date(fechaHasta, 'dd/mm/yyyy HH24:MI:SS') or fechaHasta is null) 
                          and  exists( select 1 from fa_depositos d  where d.farmacia=cvd.farmacia and
                                       d.numero_deposito=cvd.documento and d.activa='S'
                                       and (d.tipo_deposito = tipoDep or tipoDep is null))
                          and CVD.FARMACIA=ADF.CODIGO
                          ORDER BY ADF.NOMBRE;
                
BEGIN
 l_data := myTableType();
 cont:=0;
    OPEN C_PERF;
    LOOP
    cont:=cont+1;
      FETCH C_PERF INTO NOMFARM,DOCUMENTO,FARMACIAID,FECHA,VALOR,SALDO,NUMFAR;
      EXIT WHEN C_PERF%NOTFOUND;
  
  BEGIN
       select cuenta 
       INTO vn_cuenta 
       from in_registros_dep_far
       where  numero_deposito= DOCUMENTO 
       AND farmacia=FARMACIAID;
      
        select cuenta
        INTO   numero_cuenta
        from   ad_cuentas
        where  codigo=vn_cuenta;
        
          exception when others then
          numero_cuenta:='Problemas con la cuenta.';
   END;
   
    BEGIN
         select descripcion
          into tipo_dep  
          from fa_tipos_depositos t,fa_depositos d
          where t.codigo=d.tipo_deposito
          and numero_deposito= DOCUMENTO
          and farmacia= FARMACIAID
          and t.codigo<>2
          and d.activa='S';

        exception when others then
             tipo_dep  :='';
    END;
   
      l_data.extend;
      l_data(cont) := CARGA_DEPOSITO_VAR(NOMFARM,FECHA,DOCUMENTO,tipo_dep,numero_cuenta,VALOR,SALDO,NUMFAR);
    END LOOP;
    CLOSE C_PERF;
    RETURN l_data;
END;
