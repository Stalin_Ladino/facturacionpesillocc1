--PERMISOS A TABLA IN_DIFERENCIAS_CAJAS CON USUARIO WEBLINK
GRANT SELECT, INSERT, UPDATE, DELETE ON IN_DIFERENCIAS_CAJA TO weblink;


--INSERCION DENTRO DEL MENU  --LOS COGIGOS PADRE ES 14(Administracion) HIJO ES 15(Parametros Generales)
insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (15,14, 1, 'Farmacias', 'S', '/SistemaAdministrativoGPF/page/contabilidad/parametrosGenerales.jsf', 1)

insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (17,14, 1, 'Carga Deposito', 'S', '/SistemaAdministrativoGPF/page/contabilidad/cargaDeposito.jsf', 1)

insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (18,14, 1, 'Parametros Recargas', 'S', '/SistemaAdministrativoGPF/page/contabilidad/parametrosRecargas.jsf', 1)

insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (20,NULL, 1, 'VALES DE CAJA', 'N', NULL, 1);

insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (21,20, 1, 'Diferencias de Caja', 'N', NULL, 1);

insert into wf_adm_menu(CODIGO, CODIGO_PADRE, CODIGO_APLICACION, DESCRIPCION, EJECUTABLE, URL, ORDEN) 
values (22,21, 1, 'Manual', 'S', '/SistemaAdministrativoGPF/page/contabilidad/valesDeCajaManual.jsf', 1);



--ASIGNAR EL MENU A UN ROL DETERMINADO
select * from wf_adm_menu_rol
insert into wf_adm_menu_rol values (15,1)

insert into wf_adm_menu_rol values (17,1)

insert into wf_adm_menu_rol values (18,1)

insert into wf_adm_menu_rol values (20,1)

insert into wf_adm_menu_rol values (21,1)

insert into wf_adm_menu_rol values (22,1)