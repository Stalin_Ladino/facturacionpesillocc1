function start() {
    PF('statusDialog').show();
}
 
function stop() {
    PF('statusDialog').hide();
}



function valRolMOM() {
	
	var idUsu = "#frmAdmLogin\\:idRol";
	var msgIdUsu = "#frmAdmLogin\\:msgIdRol";
	var desMsgReqNomPro = "Error: Campo \"Nombre Rol\" es requerido";
	var desMsgInvNomPro = "Error: Campo \"Nombre Rol\" no es v&#225;lido debe tener un m&#237;nimo de 3 y m&#225;ximo de 10 caracteres";
	var valNomPro = true;
	
	if ($(idUsu).val() === "") {
		$(msgIdUsu).empty();
		$(msgIdUsu).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgIdUsu).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgReqNomPro + "</span>")
		$(idUsu).addClass("ui-state-error __web-inspector-hide-shortcut__")
		valNomPro = false;
	}else{
		if($(idUsu).val().length >= 3 && $(idUsu).val().length <= 10 ){
			$(idUsu).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgIdUsu).empty();
			$(msgIdUsu).removeClass( "ui-message-error ui-widget ui-corner-all" )
			valNomPro = true;
		}else{
			$(msgIdUsu).empty();
			$(msgIdUsu).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgIdUsu).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgInvNomPro + "</span>")
			$(idUsu).addClass("ui-state-error __web-inspector-hide-shortcut__")
			valNomPro = false;
		}
	}
	
	
	if( valNomPro) 
		return true; 
	else 
		return false 
}





function valUsuarioMOM() {
	
	var idUsu = "#frmAdmLogin\\:idUsu";
	var msgIdUsu = "#frmAdmLogin\\:msgIdUsu";
	var desMsgReqNomPro = "Error: Campo \"Nombre Usuario\" es requerido";
	var desMsgInvNomPro = "Error: Campo \"Nombre Usuario\" no es v&#225;lido debe tener un m&#237;nimo de 3 y m&#225;ximo de 10 caracteres";
	var valNomPro = true;
	
	if ($(idUsu).val() === "") {
		$(msgIdUsu).empty();
		$(msgIdUsu).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgIdUsu).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgReqNomPro + "</span>")
		$(idUsu).addClass("ui-state-error __web-inspector-hide-shortcut__")
		valNomPro = false;
	}else{
		if($(idUsu).val().length >= 3 && $(idUsu).val().length <= 10 ){
			$(idUsu).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgIdUsu).empty();
			$(msgIdUsu).removeClass( "ui-message-error ui-widget ui-corner-all" )
			valNomPro = true;
		}else{
			$(msgIdUsu).empty();
			$(msgIdUsu).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgIdUsu).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgInvNomPro + "</span>")
			$(idUsu).addClass("ui-state-error __web-inspector-hide-shortcut__")
			valNomPro = false;
		}
	}
	

	
	var desPro = "#frmAdmLogin\\:idNomComIng";
	var msgDesPro = "#frmAdmLogin\\:msgidNomComIng";
	var desMsgReqDesPro = "Error: Campo \"Nombre completo\" es requerido";
	var desMsgInvDesPro = "Error: Campo \"Nombre completo\" no es v&#225;lido debe tener un m&#237;nimo de 3 y m&#225;ximo de 20 caracteres";
	var valDesPro = true;
	
	if ($(desPro).val() === "") {
		$(msgDesPro).empty();
		$(msgDesPro).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgDesPro).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgReqDesPro + "</span>")
		$(desPro).addClass("ui-state-error __web-inspector-hide-shortcut__")
		valDesPro = false;
	}else{
		if($(desPro).val().length >= 3 && $(desPro).val().length <= 50 ){
			$(desPro).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgDesPro).empty();
			$(msgDesPro).removeClass( "ui-message-error ui-widget ui-corner-all" )
			valDesMen = true;
		}else{
			$(msgDesPro).empty();
			$(msgDesPro).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgDesPro).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgInvDesPro + "</span>")
			$(desPro).addClass("ui-state-error __web-inspector-hide-shortcut__")
			valDesPro = false;
		}
	}
	

	
	if(valDesPro && valNomPro) 
		return true; 
	else 
		return false 
}




// genericos 
function validaSelectOneMenu(id,idSel,msg,camp){
	var codRol = "select[name$='"+id+"_input'] option:selected";
	var divCodRol = idSel;
	var msgCodRol = msg;
	var desMsgCodRol="Error: Campo \""+camp+"\" es requerido";
	var valcodRol = true;
	if ($(codRol).val() === "0") {
		$(msgCodRol).empty();
		$(msgCodRol).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgCodRol).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgCodRol + "</span>")
		$(divCodRol).addClass("ui-state-error __web-inspector-hide-shortcut__")
		valcodRol = false;
	}else{
    	$(divCodRol).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
    	$(msgCodRol).empty();
		$(msgCodRol).removeClass( "ui-message-error ui-widget ui-corner-all" )
		valcodRol = true;
	}
} 



function validaSelectOneMenuForm(id,idSel,msg,camp) {
	var codRol = "select[name$='"+id+"_input'] option:selected";
	var divCodRol = idSel;
	var msgCodRol = msg;
	var desMsgCodRol="Error: Campo \""+camp+"\" es requerido";
	var valcodRol = true;
	if ($(codRol).val() === "0") {
		$(msgCodRol).empty();
		$(msgCodRol).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgCodRol).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgCodRol + "</span>")
		$(divCodRol).addClass("ui-state-error __web-inspector-hide-shortcut__")
		valcodRol = false;
	}else{
    	$(divCodRol).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
    	$(msgCodRol).empty();
		$(msgCodRol).removeClass( "ui-message-error ui-widget ui-corner-all" )
		valcodRol = true;
	}
	if(valcodRol) 
		return true; 
	else 
		return false 
}


function handleSubmit(args, dialog) {
    var jqDialog = jQuery('#' + dialog);
    if (args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
    } else {
        PF(dialog).hide();
    }
    
    
}


function validateEmail() {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var email = $( "input[name='frmAdmLogin:usuarioEditForm:corEle']" ).val();
    var nomInp="input[name='frmAdmLogin:usuarioEditForm:corEle']";
    var nomMsg="#frmAdmLogin\\:usuarioEditForm\\:msgCorEle";
    var desError="Error de validacion: Mail invalido";
    
	if ($(nomInp).val() === "") {
		$(nomMsg).empty();
		$(nomMsg).addClass("ui-message-error ui-widget ui-corner-all")
		$(nomMsg).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El campo Mail es requerido</span>")
		$(nomInp).addClass("ui-state-error __web-inspector-hide-shortcut__")
		
	}else{
    
	    if( emailReg.test( email ) ) {
	    	$(nomMsg).empty();
	    	$(nomMsg).removeClass( "ui-message-error ui-widget ui-corner-all" )
	    	$(nomInp).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    
	    }else {
	    	$(nomMsg).empty();
	    	$(nomMsg).addClass( "ui-message-error ui-widget ui-corner-all" )
	    	$(nomInp).addClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(nomMsg).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+desError+"</span>")
	   }
	}
 }


function validateCedula(tag, message, errorMessage) {
	
	var numIde="#"+tag;
	var msgNumId="#"+message;
	var desMsgNumId=errorMessage;
	
	if ($(numIde).val() === "") {
		$(msgNumId).empty();
		$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El campo identificacion es requerido</span>")
		$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
	}else{
		if(!validaCedula ($(numIde).val())){
			$(msgNumId).empty();
			$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>"+ desMsgNumId + "</span>")
			$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
		}else{
			$(numIde).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgNumId).empty();
			$(msgNumId).removeClass( "ui-message-error ui-widget ui-corner-all" )
		}
	}
 }



function validateLetras(tag, message, errorMessage) {
	
	var numIde="#"+tag;
	var msgNumId="#"+message;
	var desMsgNumId=errorMessage;
	
	if ($(numIde).val() === "") {
		$(msgNumId).empty();
		$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El campo "+desMsgNumId+" es requerido</span>")
		$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
	}else{
		if($(numIde).val().length >= 3 && $(numIde).val().length <= 15 ){
			$(numIde).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgNumId).empty();
			$(msgNumId).removeClass( "ui-message-error ui-widget ui-corner-all" )
		}else{
			$(msgNumId).empty();
			$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El valor del campo "+desMsgNumId+" no es valido</span>")
			$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
		}
	}
 }


function validaTelLoc(tag, message, errorMessage) {
	
	var numIde="#"+tag;
	var msgNumId="#"+message;
	var desMsgNumId=errorMessage;
	
	if ($(numIde).val() === "") {
		$(msgNumId).empty();
		$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
		$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El campo "+desMsgNumId+" es requerido</span>")
		$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
	}else{
		if($(numIde).val().length === 9 ){
			$(numIde).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgNumId).empty();
			$(msgNumId).removeClass( "ui-message-error ui-widget ui-corner-all" )
		}else{
			$(msgNumId).empty();
			$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El valor del campo "+desMsgNumId+" no es valido</span>")
			$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
		}
	}
 }

function validaTelCel(tag, message, errorMessage) {
	
	var numIde="#"+tag;
	var msgNumId="#"+message;
	var desMsgNumId=errorMessage;
	
	if ($(numIde).val() !== "") {
		
		if($(numIde).val().length === 10 ){
			$(numIde).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
	    	$(msgNumId).empty();
			$(msgNumId).removeClass( "ui-message-error ui-widget ui-corner-all" )
		}else{
			$(msgNumId).empty();
			$(msgNumId).addClass("ui-message-error ui-widget ui-corner-all")
			$(msgNumId).append("<span class='ui-message-error-icon'/><span class='ui-message-error-detail'>Error de validacion: El valor del campo "+desMsgNumId+" no es valido</span>")
			$(numIde).addClass("ui-state-error __web-inspector-hide-shortcut__")
		}
	}else{
		$(numIde).removeClass( "ui-state-error __web-inspector-hide-shortcut__" )
    	$(msgNumId).empty();
		$(msgNumId).removeClass( "ui-message-error ui-widget ui-corner-all" )
	}
 }





function validarNumeros(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    		if (charCode === 9 || charCode === 8 ||charCode === 46 || charCode ===36 || charCode ===35) {
    			return true
    		}		
    		
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false
    }
    return true
}


function validarLetras(evt) {
	evt = (evt) ? evt : window.event
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode === 9 || charCode === 8 ||charCode === 46 || charCode ===36 || charCode ===35) {
		return true
	}

	tecla = (document.all) ? evt.keyCode : evt.which;
	patron = /[A-Z a-z áéíóú]/;
	te = String.fromCharCode(tecla);
	return patron.test(te);
}


function validaCedula (numero) {          
	   
    var suma = 0;      
    var residuo = 0;      
    var pri = false;      
    var pub = false;            
    var nat = false;      
    var numeroProvincias = 22;                  
    var modulo = 11;
                
    var ok=1;
    for (i=0; i<numero.length && ok==1 ; i++){
       var n = parseInt(numero.charAt(i));
       if (isNaN(n)) ok=0;
    }
    if (ok==0){
       return false;
    }
                
    if (numero.length < 10 ){              
       return false;
    }
   
    provincia = numero.substr(0,2);      
    if (provincia < 1 || provincia > numeroProvincias){           
    	return false;       
    }

    d1  = numero.substr(0,1);         
    d2  = numero.substr(1,1);         
    d3  = numero.substr(2,1);         
    d4  = numero.substr(3,1);         
    d5  = numero.substr(4,1);         
    d6  = numero.substr(5,1);         
    d7  = numero.substr(6,1);         
    d8  = numero.substr(7,1);         
    d9  = numero.substr(8,1);         
    d10 = numero.substr(9,1);                
       
    if (d3==7 || d3==8){           
       return false;
    }         
       
    if (d3 < 6){           
       nat = true;            
       p1 = d1 * 2;  if (p1 >= 10) p1 -= 9;
       p2 = d2 * 1;  if (p2 >= 10) p2 -= 9;
       p3 = d3 * 2;  if (p3 >= 10) p3 -= 9;
       p4 = d4 * 1;  if (p4 >= 10) p4 -= 9;
       p5 = d5 * 2;  if (p5 >= 10) p5 -= 9;
       p6 = d6 * 1;  if (p6 >= 10) p6 -= 9; 
       p7 = d7 * 2;  if (p7 >= 10) p7 -= 9;
       p8 = d8 * 1;  if (p8 >= 10) p8 -= 9;
       p9 = d9 * 2;  if (p9 >= 10) p9 -= 9;             
       modulo = 10;
    }         

    else if(d3 == 6){           
       pub = true;             
       p1 = d1 * 3;
       p2 = d2 * 2;
       p3 = d3 * 7;
       p4 = d4 * 6;
       p5 = d5 * 5;
       p6 = d6 * 4;
       p7 = d7 * 3;
       p8 = d8 * 2;            
       p9 = 0;            
    }         
       
    else if(d3 == 9) {           
       pri = true;                                   
       p1 = d1 * 4;
       p2 = d2 * 3;
       p3 = d3 * 2;
       p4 = d4 * 7;
       p5 = d5 * 6;
       p6 = d6 * 5;
       p7 = d7 * 4;
       p8 = d8 * 3;
       p9 = d9 * 2;            
    }
              
    suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;                
    residuo = suma % modulo;                                         

    digitoVerificador = residuo==0 ? 0: modulo - residuo;                

    if (pub==true){           
       if (digitoVerificador != d9){                          
          return false;
       }                  
       if ( numero.substr(9,4) != '0001' ){                    
          return false;
       }
    }         
    else if(pri == true){         
       if (digitoVerificador != d10){                          
          return false;
       }         
       if ( numero.substr(10,3) != '001' ){                    
          return false;
       }
    }      

    else if(nat == true){         
       if (digitoVerificador != d10){                          
          return false;
       }         
       if (numero.length >10 && numero.substr(10,3) != '001' ){                    
          return false;
       }
    }      
    return true;   
 }    
		





