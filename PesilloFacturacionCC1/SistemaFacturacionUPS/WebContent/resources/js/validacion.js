//
// // Bloquear funcionalidad de botones en FIREFOX / CHROME
//

// Validación de numeros con decimal//
function decimales(e) {

	tecla = (document.all) ? e.keyCode : e.which;

	if (tecla == 86 && e.ctrlKey) {
		return true;
	}
	if (tecla == 67 && e.ctrlKey) {
		return true;
	}
	if (e.shiftKey) {
		return false;
	}
	if (tecla == 8) {
		return true;
	}// para la tecla de retroceso
	else if (tecla == 0 || tecla == 9) {
		return true;
	}// <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta
		// transformando a 0 asi que porsiacaso los dos
	patron = /^[0-9\.\s]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}
// Validación de solo numeros//

function numeros(e) {

	tecla = (document.all) ? e.keyCode : e.which;

	if (tecla == 86 && e.ctrlKey) {
		return true;
	}
	if (tecla == 67 && e.ctrlKey) {
		return true;
	}
	if (e.shiftKey) {
		return false;
	}
	if (tecla == 8) {
		return true;
	}// para la tecla de retroceso
	else if (tecla == 0 || tecla == 9) {
		return true;
	} // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta
		// transformando a 0 asi que porsiacaso los dos
	patron = /^[0-9]+$/;

	te = String.fromCharCode(tecla);
	return patron.test(te);

}

// Validación de solo letras y numeros//

function letras(e) {

	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8)
		return true; // para la tecla de retroceso
	else if (tecla == 0 || tecla == 9)
		return true; // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla
						// se esta transformando a 0 asi que porsiacaso los dos

	patron = /^[A-Za-z0-9\sáéíóúñ]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}
function letras1(e) {

	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8)
		return true; // para la tecla de retroceso
	else if (tecla == 0 || tecla == 9)
		return false; // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla
						// se esta transformando a 0 asi que porsiacaso los dos
	if (tecla == 13) {
		e.preventDefault();
	}
	patron = /^[A-Za-z0-9\sáéíóúñ]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}

// Validación de correo//
function correo(e) {

	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8)
		return true; // para la tecla de retroceso
	else if (tecla == 0 || tecla == 9)
		return true; // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla
						// se esta transformando a 0 asi que porsiacaso los dos

	patron = /^[A-Za-z0-9\sáéíóúñ@.\s]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}

function fechas(e) {

	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8)
		return true; // para la tecla de retroceso
	else if (tecla == 0 || tecla == 9)
		return true; // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla
						// se esta transformando a 0 asi que porsiacaso los dos

	patron = /^[ ]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}

// Validacion solo letras
function nombres(e) {

	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 8)
		return true; // para la tecla de retroceso
	else if (tecla == 0 || tecla == 9)
		return true; // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla
						// se esta transformando a 0 asi que porsiacaso los dos

	patron = /^[A-Za-z\sáéíóúñ]+$/; // -> solo caracteres en este rango
	te = String.fromCharCode(tecla);
	return patron.test(te);
}
