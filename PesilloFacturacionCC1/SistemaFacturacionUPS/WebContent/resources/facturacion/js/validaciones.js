function tipoCodigoSelector () {
	var tipo_codigo_barras = document.getElementById('frmProducto:formDatosBasicos:idTipoCodigoBarras:socc').value;
	if (tipo_codigo_barras == '900017') { // OTROS
		bandera = 1;
		idInfo = document.createElement("label");
		idInfo = document.getElementById('frmProducto:formDatosBasicos:mensajeNoticeCodigoBarra');
//		document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').disabled =  true;
		dibujarMensajeError("Se asignará al producto un código de barras manual al momento de la codificación", idInfo);
//		element.value = 0;
	}
}


function validarCodigoBarras(element) {
	var tipo_codigo_barras = document.getElementById('frmProducto:formDatosBasicos:idTipoCodigoBarras:socc').value;
	idMensaje = document.createElement("label");
	idMensaje = document.getElementById('frmProducto:formDatosBasicos:mensajeErrorCodigoBarra');

	idInfo = document.createElement("label");
	idInfo = document.getElementById('frmProducto:formDatosBasicos:mensajeNoticeCodigoBarra');

	var bandera = 0;
	if (tipo_codigo_barras == '900015') { // EAN 13
		ean13(element);
		bandera = 1;
	}
	else if (tipo_codigo_barras == '900016') { // EAN 8
		ean8(element);
		bandera = 1;
	}
	else if (tipo_codigo_barras == '900018') { // EAN 12
		ean12(element);
		bandera = 1;
	}
	else if (tipo_codigo_barras == '900017') { // OTROS
		bandera = 1;
//		document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').disabled =  true;
		dibujarMensajeError("Se asignara al producto un código de barras manual al momento de la codificación", idInfo);
		element.value = 0;
	}
	else if (bandera==0) {
		dibujarMensajeError("Debe seleccionar el tipo de c\u00F3digo de barras previamente.",idMensaje);
		element.value = '';
	}
}

function limpiarCodigoBarras() {
	document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').value='';
}

/**
 * Algoritmo de validacion para codigos de barras con codificacion aen13
 * @param elemento tipo inputText desde la vista
 */
function ean13(elemento) {
	var checksum = 0;
	var comprobador = 0;
	var elementoTmp = 0;
	var digito = 0;
	idMensaje = document.createElement("label");
	idMensaje = document.getElementById('frmProducto:formDatosBasicos:mensajeErrorCodigoBarra');
	// se verifica que el codigo de barras tenga 13 digitos 
	if (elemento.value.length == 13) {
		// obtenemos el ultimo digito
		digito = elemento.value.substring(12,13);
		elementoTmp = elemento.value.substring(0,12);
		elementoTmp = elementoTmp.split('').reverse();
		for(var pos in elementoTmp){
			checksum += elementoTmp[pos] * (3 - 2 * (pos % 2));
		}
		comprobador = ((10 - (checksum % 10 )) % 10);
		if (comprobador != digito){
			this.dibujarMensajeError("El c\u00F3digo ingresado es err\u00F3neo o no corresponde a codificaci\u00F3n seleccionada", idMensaje);
			document.getElementById(elemento.id).focus();
			elemento.value='';
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		} // En caso sea correcto
		else 
		{
			ocultarMensajeError(idMensaje);
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  false;
		}
	}
	else {
		this.dibujarMensajeError("El c\u00F3digo de barras debe contener 13 digitos", idMensaje);
		document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').focus();
		elemento.focus();
		document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		elemento.value='';
	}
}


/**
 * Validaci&oacute;n del c&oacute;digo de barras formato aen8
 * @param elemento inputText desde la vista
 */
function ean8(elemento) {
	var checksum = 0;
	var comprobador = 0;
	var elementoTmp = 0;
	var digito = 0;
	idMensaje = document.createElement("label");
	idMensaje = document.getElementById('frmProducto:formDatosBasicos:mensajeErrorCodigoBarra');
	// se verifica que el codigo de barras tenga 13 digitos 
	if (elemento.value.length == 8) {
		// obtenemos el ultimo digito
		digito = elemento.value.substring(7,8);
		elementoTmp = elemento.value.substring(0,7);
		elementoTmp = elementoTmp.split('').reverse();
		for(var pos in elementoTmp){
			checksum += elementoTmp[pos] * (3 - 2 * (pos % 2));
		}
		comprobador = ((10 - (checksum % 10 )) % 10);
		if (comprobador != digito){
			this.dibujarMensajeError("El c\u00F3digo ingresado es err\u00F3neo o no corresponde a codificaci\u00F3n seleccionada", idMensaje);
			document.getElementById(element.id).focus();
			elemento.value='';
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		} // En caso sea correcto
		else {
			ocultarMensajeError(idMensaje);
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  false;
		}
	}
	else {
		this.dibujarMensajeError("El c\u00F3digo de barras debe contener 8 digitos", idMensaje);
		document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').focus();
		document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		elemento.focus();
		elemento.value='';
	}
}

/**
 * Validaci&oacute;n del c&oacute;digo de barras formato aen8
 * @param elemento inputText desde la vista
 */
function ean12(elemento) {
	var checksum = 0;
	var comprobador = 0;
	var elementoTmp = 0;
	var digito = 0;
	idMensaje = document.createElement("label");
	idMensaje = document.getElementById('frmProducto:formDatosBasicos:mensajeErrorCodigoBarra');
	// se verifica que el codigo de barras tenga 13 digitos 
	if (elemento.value.length == 12) {
		// obtenemos el ultimo digito
		digito = elemento.value.substring(11,12);
		elementoTmp = elemento.value.substring(0,11);
		elementoTmp = elementoTmp.split('').reverse();
		for(var pos in elementoTmp){
			checksum += elementoTmp[pos] * (3 - 2 * (pos % 2));
			alert ("suma checksum " + checksum + " posicion " + pos);
		}
		
		
		comprobador = ((10 - (checksum % 10 )) % 10);
		if (comprobador != digito){
			this.dibujarMensajeError("El c\u00F3digo ingresado es err\u00F3neo o no corresponde a codificaci\u00F3n seleccionada", idMensaje);
			document.getElementById(element.id).focus();
			elemento.value='';
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		} // En caso sea correcto
		else {
			ocultarMensajeError(idMensaje);
			document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  false;
		}
	}
	else {
		this.dibujarMensajeError("El c\u00F3digo de barras debe contener 12 digitos", idMensaje);
		document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').focus();
		document.getElementById('frmProducto:formDatosBasicos:btnCrearProducto').disabled =  true;
		elemento.focus();
		elemento.value='';
	}
}


/**
 * Dibuja un mensaje de error 
 * @param error
 */
function dibujarMensajeError(mensajeError,elemento) {
	document.getElementById(elemento.id).style.display = 'block';
	document.getElementById(elemento.id).innerHTML= mensajeError;
}

function ocultarMensajeError(elemento) {
	document.getElementById(elemento.id).style.display = 'none';
}

function cambiarMayuscula(elemento, e) {
	tecla=(document.all) ? e.keyCode : e.which;
	if (elemento!=null) 
		elemento.value = elemento.value.toUpperCase();
}

function soloNumeros(e) {
	var key = (document.all) ? e.keyCode : e.which; 
	return ((key >= 48 && key < 58) || (key==8) || (key==46))
}

function soloNumerosEnteros(elemento) {
	if (elemento!=null && elemento.value==0) 
		elemento.value = 1;
}


function validarEmail( elemento ) {
	expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if ( !expr.test(elemento.value) )
		alert(" Dirección de correo eletrónico es incorrecta.");
}

function validarDireccion( elemento ) {
	if (elemento.value.length > 80){
		alert(elemento.value.length+"Carácteres de 80, únicamente se permiten 80 caracteres");
		return false
	}else{
		return true 
	}
}

function ocultarPanel() {
	$(".panel .inside .box  div.ui-layout-center").css("width","100%");
	$(".panel .inside .box  div.ui-layout-west").hide();
	}

function mostrarPanel() {
	$(".panel .inside .box  div.ui-layout-west").slideDown("slow");
	$(".panel .inside .box  div.ui-layout-center").css("width","75%");
	}

$(".cancelar").click(function(event){
	if ($(".panel .inside .box  div.ui-layout-west").is(":hidden")) {
		$(".panel .inside .box  div.ui-layout-west").slideDown("slow");
		$(".panel .inside .box  div.ui-layout-center").css("width","75%");
	} else {
		
	}
});




//function limitarTamano(elemento, limite) {
////	var limite = 250;
//	
//	var contenido = document.getElementById('frmProducto:formDatosBasicos:descripcionCompleta:itacc').value;
//	var cantidad = document.getElementById('frmProducto:formDatosBasicos:descripcionCompleta:itacc').value.length;
//	
//	document.getElementById('frmProducto:formDatosBasicos:contadorDescripcionProduxto').value=cantidad;
//	if(cantidad>limite)
//	{
//	document.getElementById('frmProducto:formDatosBasicos:descripcionCompleta:itacc').value=contenido.substring(0, limite);
//	cantidad = document.getElementById('frmProducto:formDatosBasicos:descripcionCompleta:itacc').value.length;
//	document.getElementById('frmProducto:formDatosBasicos:contadorDescripcionProduxto').value=cantidad;
//	return false;
//}
//}



function limitarTamano(elemento, limite) {
//	var limite = 250;
	var contenido = elemento.value;
	var cantidad = elemento.value.length;
	console.log("cantidad" + cantidad);
	elemento.maxlength = limite;
	//	elemento.value=cantidad;
	if(cantidad>limite)
	{
		elemento.value=contenido.substring(0, limite);
		cantidad = elemento.value.length;
		return false;
}
}



//$( document ).ready(function() {
//	var codigo_barras_tipo = document.getElementById('frmProducto:formDatosBasicos:idTipoCodigoBarras:socc').value;
//	if (codigo_barras_tipo=='900017') {
//	document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').disabled =  true;
//	document.getElementById('frmProducto:formDatosBasicos:txtCodigoBarras:itcc').value =  0;
//	}
//});