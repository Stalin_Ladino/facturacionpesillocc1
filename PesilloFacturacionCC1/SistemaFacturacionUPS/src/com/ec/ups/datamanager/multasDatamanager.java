/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblMulta;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="multasDatamanager")
@SessionScoped
public class multasDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private TblMulta mulFormul;
	
	private List<TblMulta> mulList;
	
	private TblMulta mulSeleccion;

	private String verPantalla;
	
	private String accion;
	
	private List<secComDTO> secComList;
	
	private List<usuarioDTO> usuarioList;
	
	private List<usuarioDTO> usuarioSelectList;
	
	private List<TblUsuarioMulta> usuarioMulList;
	
	private TblUsuarioMulta usuarioMulSelec;
	
	//Lista sector Usuario
	private List<TblSectorUsuario> sectorUsuarioList;
	
	//Sector Usuario Seleccionado
	private List<TblSectorUsuario> sectorUserSeleccionado;
	
	public TblMulta getmulFormul() {
		return mulFormul;
	}

	public void setmulFormul(TblMulta mulFormul) {
		this.mulFormul = mulFormul;
	}

	public List<TblMulta> getmulList() {
		return mulList;
	}

	public void setmulList(List<TblMulta> mulList) {
		this.mulList = mulList;
	}

	public TblMulta getmulSeleccion() {
		if(null==mulSeleccion){
			mulSeleccion=new TblMulta();
		}
		return mulSeleccion;
	}

	public void setmulSeleccion(TblMulta mulSeleccion) {
		this.mulSeleccion = mulSeleccion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public List<usuarioDTO> getUsuarioList() {
		return usuarioList;
	}

	public void setUsuarioList(List<usuarioDTO> usuarioList) {
		this.usuarioList = usuarioList;
	}

	public List<usuarioDTO> getUsuarioSelectList() {
		return usuarioSelectList;
	}

	public void setUsuarioSelectList(List<usuarioDTO> usuarioSelectList) {
		this.usuarioSelectList = usuarioSelectList;
	}

	public List<TblUsuarioMulta> getUsuarioMulList() {
		return usuarioMulList;
	}

	public void setUsuarioMulList(List<TblUsuarioMulta> usuarioMulList) {
		this.usuarioMulList = usuarioMulList;
	}

	public TblUsuarioMulta getUsuarioMulSelec() {
		return usuarioMulSelec;
	}

	public void setUsuarioMulSelec(TblUsuarioMulta usuarioMulSelec) {
		this.usuarioMulSelec = usuarioMulSelec;
	}

	public List<TblSectorUsuario> getSectorUsuarioList() {
		return sectorUsuarioList;
	}

	public void setSectorUsuarioList(List<TblSectorUsuario> sectorUsuarioList) {
		this.sectorUsuarioList = sectorUsuarioList;
	}

	public List<TblSectorUsuario> getSectorUserSeleccionado() {
		return sectorUserSeleccionado;
	}

	public void setSectorUserSeleccionado(
			List<TblSectorUsuario> sectorUserSeleccionado) {
		this.sectorUserSeleccionado = sectorUserSeleccionado;
	}


	
	
}
