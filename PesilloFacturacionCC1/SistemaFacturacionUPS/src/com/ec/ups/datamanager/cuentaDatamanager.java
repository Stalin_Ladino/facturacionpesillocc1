/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="cuentaDatamanager")
@SessionScoped
public class cuentaDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	
	private String contrasena;
	
	private usuarioDTO usuarioFormul;

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public usuarioDTO getUsuarioFormul() {
		return usuarioFormul;
	}

	public void setUsuarioFormul(usuarioDTO usuarioFormul) {
		this.usuarioFormul = usuarioFormul;
	}

	
	
	
}
