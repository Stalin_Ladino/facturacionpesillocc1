/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.ResultadoFactura;
import com.ec.ups.jpa.entidades.TblEvento;
import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.jpa.entidades.TblMulta;
import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla georeferenciacion
 * @author sladino
 *
 */
@ManagedBean(name="georeferenciacionDatamanager")
@SessionScoped
public class georeferenciacionDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	private String verPantalla;
	
	private String accion;
	
	private List<TblEvento> listEventos;
	
	private TblEvento formulEventos;
	
	private TblEvento eventSelect;


	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<TblEvento> getListEventos() {
		return listEventos;
	}

	public void setListEventos(List<TblEvento> listEventos) {
		this.listEventos = listEventos;
	}

	public TblEvento getFormulEventos() {
		return formulEventos;
	}

	public void setFormulEventos(TblEvento formulEventos) {
		this.formulEventos = formulEventos;
	}

	public TblEvento getEventSelect() {
		return eventSelect;
	}

	public void setEventSelect(TblEvento eventSelect) {
		this.eventSelect = eventSelect;
	}
	
	
}
