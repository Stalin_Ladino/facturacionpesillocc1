/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.FkRolPagina;
import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.jpa.entidades.rolDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="rolesDatamanager")
@SessionScoped
public class rolesDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private rolDTO rolDTO;
	
	private List<rolDTO> rolList;
	
	private String verPantalla;
	
	private String accion;
	
	private rolDTO rolSelect;
	
	private List<FkRolPagina> fkRolPaginaList;
	
	private TblPagina tblPagina;
	
	private List<TblPagina> tblPaginaList;
	
	private List<TblPagina> paginaListSelect;

	public rolDTO getRolDTO() {
		return rolDTO;
	}

	public void setRolDTO(rolDTO rolDTO) {
		this.rolDTO = rolDTO;
	}

	public List<rolDTO> getRolList() {
		return rolList;
	}

	public void setRolList(List<rolDTO> rolList) {
		this.rolList = rolList;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public rolDTO getRolSelect() {
		return rolSelect;
	}

	public void setRolSelect(rolDTO rolSelect) {
		this.rolSelect = rolSelect;
	}

	public TblPagina getTblPagina() {
		return tblPagina;
	}

	public void setTblPagina(TblPagina tblPagina) {
		this.tblPagina = tblPagina;
	}

	public List<TblPagina> getTblPaginaList() {
		return tblPaginaList;
	}

	public void setTblPaginaList(List<TblPagina> tblPaginaList) {
		this.tblPaginaList = tblPaginaList;
	}

	public List<TblPagina> getPaginaListSelect() {
		return paginaListSelect;
	}

	public void setPaginaListSelect(List<TblPagina> paginaListSelect) {
		this.paginaListSelect = paginaListSelect;
	}

	public List<FkRolPagina> getFkRolPaginaList() {
		return fkRolPaginaList;
	}

	public void setFkRolPaginaList(List<FkRolPagina> fkRolPaginaList) {
		this.fkRolPaginaList = fkRolPaginaList;
	}


	
	
}
