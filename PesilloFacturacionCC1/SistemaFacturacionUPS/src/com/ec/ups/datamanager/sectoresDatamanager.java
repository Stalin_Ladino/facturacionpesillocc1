/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblRegional;
import com.ec.ups.jpa.entidades.secComDTO;;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="sectoresDatamanager")
@SessionScoped
public class sectoresDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private  secComDTO sectorFormul;
	
	private List<TblRegional>regionalList;
	
	private List<secComDTO>comList;
	
	private secComDTO sectoresSeleccion;

	private String verPantalla;
	
	private String accion;

	public secComDTO getSectorFormul() {
		return sectorFormul;
	}

	public void setSectorFormul(secComDTO sectorFormul) {
		this.sectorFormul = sectorFormul;
	}

	public List<TblRegional> getRegionalList() {
		return regionalList;
	}

	public void setRegionalList(List<TblRegional> regionalList) {
		this.regionalList = regionalList;
	}

	public List<secComDTO> getComList() {
		return comList;
	}

	public void setComList(List<secComDTO> comList) {
		this.comList = comList;
	}

	public secComDTO getSectoresSeleccion() {
		return sectoresSeleccion;
	}

	public void setSectoresSeleccion(secComDTO sectoresSeleccion) {
		this.sectoresSeleccion = sectoresSeleccion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
	

	
	

}
