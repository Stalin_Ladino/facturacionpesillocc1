/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.ec.ups.jpa.entidades.TblEvento;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="indexDatamanager")
@ViewScoped
public class indexDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	
    private List<TblEvento> listEventos;

//	public List<String> getImages() {
//		if(null==images || images.size()==0){
//			images= new ArrayList<String>();
//		}
//		return images;
//	}
//
//	public void setImages(List<String> images) {
//		this.images = images;
//	}

	public List<TblEvento> getListEventos() {
		return listEventos;
	}

	public void setListEventos(List<TblEvento> listEventos) {
		this.listEventos = listEventos;
	}

}
