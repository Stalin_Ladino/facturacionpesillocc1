/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.secComDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="reportesDatamanager")
@SessionScoped
public class reportesDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	
	private String cedula;
	
	private secComDTO secComSelect;
	
	private List<secComDTO> secComList;
	
	private TblServicio servicioSelect;
	
	private Date fechaIni;
	
	private Date fechaFin;
	
	private Integer tipReporte;
	
	private Boolean habilitarCedula;

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public secComDTO getSecComSelect() {
		return secComSelect;
	}

	public void setSecComSelect(secComDTO secComSelect) {
		if(null==secComSelect){
			secComSelect=new secComDTO();
		}
		this.secComSelect = secComSelect;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public TblServicio getServicioSelect() {
		return servicioSelect;
	}

	public void setServicioSelect(TblServicio servicioSelect) {
		this.servicioSelect = servicioSelect;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Integer getTipReporte() {
		return tipReporte;
	}

	public void setTipReporte(Integer tipReporte) {
		this.tipReporte = tipReporte;
	}

	public Boolean getHabilitarCedula() {
		return habilitarCedula;
	}

	public void setHabilitarCedula(Boolean habilitarCedula) {
		this.habilitarCedula = habilitarCedula;
	}
	

}
