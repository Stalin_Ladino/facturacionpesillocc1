/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.rolDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="usuarioDatamanager")
@SessionScoped
public class usuarioDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private usuarioDTO usuarioFormul;
	
	private rolDTO rolSelect;
	
	private List<rolDTO> rolList;
	
	private List<secComDTO> secComList;
	
	private List<usuarioDTO> usuarioList;
	
	//Lista sector Usuario
	private List<TblSectorUsuario> sectorUsuarioList;
	
	//Sector Usuario Seleccionado
	private TblSectorUsuario sectorUserSeleccionado;
	
	private usuarioDTO usuarioSeleccion;
	
	private String verPantalla;
	
	private String accion;
	
	private loginDTO loginFormul;
	
	private String contrasena;
	
	private Boolean existUser;
	
	private usuarioDTO existUserObj;
	
	private TblSectorUsuario tblSectorUsuarioFormul;
	
	private Boolean validarCedula;
	
	public usuarioDTO getUsuarioFormul() {
		if(null==usuarioFormul.getLoginDTO()){
			usuarioFormul.setLoginDTO(new loginDTO());
		}
			
		return usuarioFormul;
	}

	public void setUsuarioFormul(usuarioDTO usuarioFormul) {
		this.usuarioFormul = usuarioFormul;
	}

	public rolDTO getRolSelect() {
		return rolSelect;
	}

	public void setRolSelect(rolDTO rolSelect) {
		this.rolSelect = rolSelect;
	}

	public List<rolDTO> getRolList() {
		return rolList;
	}

	public void setRolList(List<rolDTO> rolList) {
		this.rolList = rolList;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public List<usuarioDTO> getUsuarioList() {
		return usuarioList;
	}

	public void setUsuarioList(List<usuarioDTO> usuarioList) {
		this.usuarioList = usuarioList;
	}

	public usuarioDTO getUsuarioSeleccion() {
		return usuarioSeleccion;
	}

	public void setUsuarioSeleccion(usuarioDTO usuarioSeleccion) {
		this.usuarioSeleccion = usuarioSeleccion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public loginDTO getLoginFormul() {
		return loginFormul;
	}

	public void setLoginFormul(loginDTO loginFormul) {
		this.loginFormul = loginFormul;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Boolean getExistUser() {
		return existUser;
	}

	public void setExistUser(Boolean existUser) {
		this.existUser = existUser;
	}

	public usuarioDTO getExistUserObj() {
		return existUserObj;
	}

	public void setExistUserObj(usuarioDTO existUserObj) {
		this.existUserObj = existUserObj;
	}

	public TblSectorUsuario getTblSectorUsuarioFormul() {
		return tblSectorUsuarioFormul;
	}

	public void setTblSectorUsuarioFormul(TblSectorUsuario tblSectorUsuarioFormul) {
		this.tblSectorUsuarioFormul = tblSectorUsuarioFormul;
	}

	public List<TblSectorUsuario> getSectorUsuarioList() {
		return sectorUsuarioList;
	}

	public void setSectorUsuarioList(List<TblSectorUsuario> sectorUsuarioList) {
		this.sectorUsuarioList = sectorUsuarioList;
	}

	public TblSectorUsuario getSectorUserSeleccionado() {
		return sectorUserSeleccionado;
	}

	public void setSectorUserSeleccionado(TblSectorUsuario sectorUserSeleccionado) {
		this.sectorUserSeleccionado = sectorUserSeleccionado;
	}

	public Boolean getValidarCedula() {
		return validarCedula;
	}

	public void setValidarCedula(Boolean validarCedula) {
		this.validarCedula = validarCedula;
	}

}
