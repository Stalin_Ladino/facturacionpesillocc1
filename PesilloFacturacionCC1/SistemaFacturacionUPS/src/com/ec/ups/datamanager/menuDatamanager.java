/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblPagina;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="menuDatamanager")
@SessionScoped
public class menuDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;

	private List<TblPagina> tblPaginaList;


	public List<TblPagina> getTblPaginaList() {
		return tblPaginaList;
	}



	public void setTblPaginaList(List<TblPagina> tblPaginaList) {
		this.tblPaginaList = tblPaginaList;
	}
	
	
}
