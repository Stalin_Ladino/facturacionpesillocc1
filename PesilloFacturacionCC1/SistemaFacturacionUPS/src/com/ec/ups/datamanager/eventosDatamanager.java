/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.UploadedFile;

import com.ec.ups.jpa.entidades.TblEvento;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="eventosDatamanager")
@SessionScoped
public class eventosDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	private String verPantalla;
	
	private String accion;
	
	private List<TblEvento> listEventos;
	
	private TblEvento formulEventos;
	
	private TblEvento eventSelect;
	
	private UploadedFile file;
	
	private String imagenes;


	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<TblEvento> getListEventos() {
		return listEventos;
	}

	public void setListEventos(List<TblEvento> listEventos) {
		this.listEventos = listEventos;
	}

	public TblEvento getFormulEventos() {
		return formulEventos;
	}

	public void setFormulEventos(TblEvento formulEventos) {
		this.formulEventos = formulEventos;
	}

	public TblEvento getEventSelect() {
		return eventSelect;
	}

	public void setEventSelect(TblEvento eventSelect) {
		this.eventSelect = eventSelect;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getImagenes() {
		return imagenes;
	}

	public void setImagenes(String imagenes) {
		this.imagenes = imagenes;
	}
	
}
