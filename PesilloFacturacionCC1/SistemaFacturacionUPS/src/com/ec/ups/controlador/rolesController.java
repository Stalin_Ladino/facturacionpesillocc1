package com.ec.ups.controlador;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.rolesDatamanager;
import com.ec.ups.ejb.interfac.impl.multaDTOInterface;
import com.ec.ups.ejb.interfac.impl.paginaDTOInterface;
import com.ec.ups.ejb.interfac.impl.rolDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.sectorUsuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioMultaDTOInterface;
import com.ec.ups.jpa.entidades.FkRolPagina;
import com.ec.ups.jpa.entidades.FkRolPaginaPK;
import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.jpa.entidades.rolDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "rolesController")
@ViewScoped
public class rolesController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{rolesDatamanager}")
	private rolesDatamanager rolesDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private multaDTOInterface multaDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private usuarioMultaDTOInterface usuarioMultaDTOInterface;
	
	@EJB
	private sectorUsuarioDTOInterface sectorUsuarioDTOInterface;
	
	@EJB
	private rolDTOInterface rolDTOInterface;
	
	@EJB
	private paginaDTOInterface paginaDTOInterface;

	

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			rolesDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			rolesDatamanager.setRolDTO(new rolDTO());
			rolesDatamanager.setRolSelect(new rolDTO());
			rolesDatamanager.setRolList(rolDTOInterface.findAll());
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void guardar(){
		if(!rolesDatamanager.getVerPantalla().equals(ConstantesUtil.PANTALLA_MODIFICAR)){
			rolDTOInterface.create(rolesDatamanager.getRolDTO());
			rolesDatamanager.setRolList(rolDTOInterface.findAll());
			this.accion(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Rol registrado con �xito");
		} else {
			rolDTOInterface.edit(rolesDatamanager.getRolDTO());
			rolesDatamanager.setRolList(rolDTOInterface.findAll());
			this.accion(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Rol actualizado con �xito");
		}

		
	}
	
	public void modificar(rolDTO rolObj){
		rolesDatamanager.setTblPaginaList(paginaDTOInterface.findAll());
		rolesDatamanager.setRolDTO(rolObj);
		rolesDatamanager.setFkRolPaginaList(paginaDTOInterface.findAllRolPagina(rolObj.getIdValuenivel()));
		for(FkRolPagina list:rolesDatamanager.getFkRolPaginaList()){
			for(TblPagina pagList:rolesDatamanager.getTblPaginaList()){
				if(list.getId().getIdPag()==pagList.getIdPag()){
					pagList.setSelectPagina(Boolean.TRUE);
				}
			}
		}
		
		this.accion(ConstantesUtil.PANTALLA_MODIFICAR);
		
	}
	
	public void eliminar(rolDTO rolObj){
		
	}
	
	public void eliminarRol(TblPagina pagina){
		FkRolPagina rolPagina=new FkRolPagina();
		rolPagina.setId(new FkRolPaginaPK());
		rolPagina.getId().setIdPag(pagina.getIdPag());
		rolPagina.getId().setIdValuenivel(rolesDatamanager.getRolDTO().getIdValuenivel());
		
		if(pagina.getSelectPagina()){
			paginaDTOInterface.createRolPagina(rolPagina);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Men� asignado con �xito");
		} else {
			paginaDTOInterface.removeRolPagina(rolPagina);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Men� privado con �xito");
		}
	}
	
	public void cancelar(String pantalla){
		this.accion(pantalla);
		rolesDatamanager.setRolList(rolDTOInterface.findAll());
		rolesDatamanager.setTblPaginaList(paginaDTOInterface.findAll());
		
	}
	
	
	public void accion(String pantalla){
		rolesDatamanager.setVerPantalla(pantalla);
	}
	
	public rolesDatamanager getrolesDatamanager() {
		return rolesDatamanager;
	}

	public void setrolesDatamanager(rolesDatamanager rolesDatamanager) {
		this.rolesDatamanager = rolesDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
