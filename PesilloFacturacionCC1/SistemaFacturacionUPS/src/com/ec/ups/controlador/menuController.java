package com.ec.ups.controlador;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.menuDatamanager;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.paginaDTOInterface;
import com.ec.ups.jpa.entidades.secComDTO;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name="menuController")
@ViewScoped
public class menuController extends CommonControlador{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean init=Boolean.TRUE;
	
	@ManagedProperty(value = "#{menuDatamanager}") 
	private menuDatamanager menuDatamanager;
	
	@ManagedProperty(value = "#{loginDatamanager}") 
	private loginDatamanager loginDatamanager;
	
	@EJB
	private loginDTOInterface loginDTOInterface;
	
	@EJB
	private paginaDTOInterface paginaDTOInterface;
	
	public void ingresar() throws IOException{
	}
	

	@Override
	public void inicializarDatos() {
		
		try {
			this.returnPage();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(init && null != loginDatamanager.getLoginObj().getIdValuenivel()){
		init=Boolean.FALSE;
		loginDatamanager.setTblPaginaList(paginaDTOInterface.findAllByLevel(loginDatamanager.getLoginObj().getIdValuenivel()));
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}
	
	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}



	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}


	public menuDatamanager getMenuDatamanager() {
		return menuDatamanager;
	}


	public void setMenuDatamanager(menuDatamanager menuDatamanager) {
		this.menuDatamanager = menuDatamanager;
	}


	
	
}
