package com.ec.ups.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.sectoresDatamanager;
import com.ec.ups.ejb.interfac.impl.regionalDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.sectorUsuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.TblRegional;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "sectoresController")
@ViewScoped
public class sectoresController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{sectoresDatamanager}")
	private sectoresDatamanager sectoresDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}") 
	private loginDatamanager loginDatamanager;
	
	@EJB
	private secComDTOInterface secComDTOInterface;
	
	@EJB
	private regionalDTOInterface regionalDTOInterface ;
	
	@EJB
	private usuarioDTOInterface usuarioDTOInterface ;
	
	@EJB
	private sectorUsuarioDTOInterface sectorUsuarioDTOInterface ;
	
	
	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			sectoresDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			if(loginDatamanager.getLoginObj().getIdValuenivel()!= ConstantesUtil.NIVEL_SUPER_ADMI){
				List<secComDTO> list=new ArrayList<secComDTO>();
				list.add(secComDTOInterface.findById(loginDatamanager.getSectorSeleSec().getIdSec()));
				sectoresDatamanager.setComList(list);
			} else {
				sectoresDatamanager.setComList(secComDTOInterface.findAll());
			}
			sectoresDatamanager.setSectoresSeleccion(new secComDTO());
			sectoresDatamanager.getSectoresSeleccion().setTblRegional(new TblRegional());
			sectoresDatamanager.setRegionalList(regionalDTOInterface.findAll());
			sectoresDatamanager.setSectorFormul(new secComDTO());
			
		}

	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	
	public void guardar() {
		if(sectoresDatamanager.getAccion().equals(ConstantesUtil.ACCION_NUEVO)){
			 Integer cod=
					 sectoresDatamanager.getSectoresSeleccion().getTblRegional().getIdReg();
					 sectoresDatamanager.getSectoresSeleccion().setIdReg(cod);
		secComDTOInterface.create(sectoresDatamanager.getSectoresSeleccion());
		sectoresDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
		
				"Sector/Comunidad registrado con �xito");
		 } else
		 if
		 (sectoresDatamanager.getAccion().equals(ConstantesUtil.ACCION_MODIFICAR))
		 {
			 Integer cod=
			 sectoresDatamanager.getSectoresSeleccion().getTblRegional().getIdReg();
			 sectoresDatamanager.getSectoresSeleccion().setIdReg(cod);
			 secComDTOInterface.edit(sectoresDatamanager.getSectoresSeleccion());
			 sectoresDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
		 JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
		 "sector/Comunidad modificado con �xito");
		 }
		if(loginDatamanager.getLoginObj().getIdValuenivel()!= ConstantesUtil.NIVEL_SUPER_ADMI){
			List<secComDTO> list=new ArrayList<secComDTO>();
			list.add(secComDTOInterface.findById(loginDatamanager.getSectorSeleSec().getIdSec()));
			sectoresDatamanager.setComList(list);
		} else {
			sectoresDatamanager.setComList(secComDTOInterface.findAll());
		}
	}

	public void accion(String pantalla) {
		sectoresDatamanager.setVerPantalla(pantalla);
		sectoresDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		sectoresDatamanager.setSectoresSeleccion(new secComDTO());
		sectoresDatamanager.getSectoresSeleccion().setTblRegional(new TblRegional());
		
	}

	public void modificar(secComDTO sectoresSelec) {
		sectoresDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		sectoresDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		sectoresDatamanager.setSectoresSeleccion(sectoresSelec);

	}

	public void eliminar(secComDTO  sectoresSelec) {
		sectorUsuarioDTOInterface.eliminarDelSec(sectoresSelec.getIdSec());
		secComDTOInterface.remove(sectoresSelec);
		
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Sector/Comunidad eliminado con �xito");
		if(loginDatamanager.getLoginObj().getIdValuenivel()!= ConstantesUtil.NIVEL_SUPER_ADMI){
			List<secComDTO> list=new ArrayList<secComDTO>();
			list.add(secComDTOInterface.findById(loginDatamanager.getSectorSeleSec().getIdSec()));
			sectoresDatamanager.setComList(list);
		} else {
			sectoresDatamanager.setComList(secComDTOInterface.findAll());
		}
	}
	public sectoresDatamanager getUsuarioDatamanager() {
		return sectoresDatamanager;
	}
	public void setsectoresDatamanager(sectoresDatamanager sectoresDatamanager) {
		this.sectoresDatamanager = sectoresDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}
	
	

}
