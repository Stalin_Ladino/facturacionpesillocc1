package com.ec.ups.controlador;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.indexDatamanager;
import com.ec.ups.ejb.interfac.impl.eventosDTOInterface;

@ManagedBean(name = "indexController")
public class indexController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{indexDatamanager}")
	private indexDatamanager indexDatamanager;

	@EJB
	private eventosDTOInterface eventosDTOInterface;

	private List<String> images;

	public void inicializarDatos() {
		if (init) {

			images = new ArrayList<String>();

			for (int i = 1; i <= 6; i++) {
				images.add("imagen" + i + ".jpg");
			}
			init = Boolean.FALSE;
			this.cargarEventos();
		}
	}

	public void cargarEventos() {
		indexDatamanager.setListEventos(eventosDTOInterface.findAll());
	}

	public indexDatamanager getIndexDatamanager() {
		return indexDatamanager;
	}

	public void setIndexDatamanager(indexDatamanager indexDatamanager) {
		this.indexDatamanager = indexDatamanager;
	}

	public List<String> getImages() {
		return images;
	}

}
