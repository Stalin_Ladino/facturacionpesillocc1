package com.ec.ups.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.CloseEvent;

import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.paginaDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.sectorUsuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;
import com.facturacion.util.web.MensajesUtil;

@ManagedBean(name = "loginController")
@SessionScoped
public class loginController {

	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private loginDTOInterface loginDTOInterface;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private paginaDTOInterface paginaDTOInterface;

	@EJB
	private sectorUsuarioDTOInterface sectorUsuarioDTOInterface;

	public void ingresar() throws IOException {
		loginDTO login = new loginDTO();
		loginDatamanager.setMapUserSec(new HashMap<>());
		loginDatamanager.setListSectores(new ArrayList<secComDTO>());
		login = loginDTOInterface.buscarLogin(
				loginDatamanager.getNombreUsuario().trim(),
				loginDatamanager.getPassword().trim());
		if (null == login) {
			JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR, MensajesUtil
					.getString("etiqueta.mensaje.credenciales.incorrectas"));
			return;
		} else {
			loginDatamanager.setSectorSelcUser(new secComDTO());
			loginDatamanager.setLoginObj(login);
			List<TblSectorUsuario> listSec = sectorUsuarioDTOInterface
					.buscarLogin(login.getIdLogin());
			if (!listSec.isEmpty()) {

				if (listSec.get(0).getIdValuenivel().equals(ConstantesUtil.NIVEL_SUPER_ADMI)) {
					System.out.println("Administrador");
				} else {
					usuarioDTO user=usuarioDTOInterface.findByIdLogin(login.getIdLogin());
					for (TblSectorUsuario listUserSec : listSec) {
						loginDatamanager.getListSectores().add(listUserSec.getSecComDTO());
						loginDatamanager.getMapUserSec().put(listUserSec.getIdSec(), listUserSec);
					}
					if(null!=user){
						loginDatamanager.setUsuarioSession(user);
					}
					FacesContext fc = FacesContext.getCurrentInstance();
					fc.getExternalContext().redirect(
							"/SistemaFacturacion/page/menu.jsf");
				}
			} else {
				loginDatamanager.getLoginObj().setIdValuenivel(ConstantesUtil.NIVEL_SUPER_ADMI);
				FacesContext fc = FacesContext.getCurrentInstance();
				fc.getExternalContext().redirect(
						"/SistemaFacturacion/page/menu.jsf");
			}
		}

	}

	public void logout() throws IOException {
		JsfUtil.removeManagedBean("loginDataManager");
		JsfUtil.invalidateSession();
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.getExternalContext().redirect(
				"/SistemaFacturacion/page/index2.jsf");
	}

	/**
	 * Metodo para instanciar las variables cuando cierro popUp
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void eventoCerrar(CloseEvent event) throws Exception {
		this.logout();
	}

	public void guardarSector() throws IOException {

		for (secComDTO list : loginDatamanager.getListSectores()) {
			if (list.getIdSec().equals(
					loginDatamanager.getSectorSelcUser().getIdSec())) {
				loginDatamanager.setSectorSeleSec(list);
			}
		}

		loginDTO login = new loginDTO();
		login.setIdValuenivel(loginDatamanager.getMapUserSec()
				.get(loginDatamanager.getSectorSelcUser().getIdSec())
				.getIdValuenivel());

		loginDatamanager.getLoginObj().setIdValuenivel(login.getIdValuenivel());
		loginDatamanager.setTblPaginaList(paginaDTOInterface.findAllByLevel(loginDatamanager.getLoginObj().getIdValuenivel()));

	}

	/**
	 * Metodo ajax para seleccionar el sector
	 * 
	 * @param event
	 *            change al cambiar seleccion del combo
	 */
	public void seleccionarSector(AjaxBehaviorEvent event) {
		loginDatamanager.getSectorSelcUser();
		for (secComDTO list : loginDatamanager.getListSectores()) {
			if (list.getIdSec().equals(
					loginDatamanager.getSectorSelcUser().getIdSec())) {
				loginDatamanager.setSectorSeleSec(list);
			}
		}
		
		loginDTO login = new loginDTO();
		login.setIdValuenivel(loginDatamanager.getMapUserSec()
				.get(loginDatamanager.getSectorSelcUser().getIdSec())
				.getIdValuenivel());

		loginDatamanager.getLoginObj().setIdValuenivel(login.getIdValuenivel());
		loginDatamanager.setTblPaginaList(paginaDTOInterface.findAllByLevel(loginDatamanager.getLoginObj().getIdValuenivel()));

	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
