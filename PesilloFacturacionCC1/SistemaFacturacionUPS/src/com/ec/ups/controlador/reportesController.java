package com.ec.ups.controlador;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.reportesDatamanager;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;
import com.facturacion.util.web.ReportServlet;

@ManagedBean(name = "reportesController")
@ViewScoped
public class reportesController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{reportesDatamanager}")
	private reportesDatamanager reportesDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private secComDTOInterface secComDTOInterface;
	
	@EJB
	private usuarioDTOInterface usuarioDTOInterface;
	

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			reportesDatamanager.setSecComSelect(new secComDTO());
			reportesDatamanager.setHabilitarCedula(Boolean.FALSE);
			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				reportesDatamanager.setSecComList(secComDTOInterface.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
				
				if(loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_ADMINISTRADOR){
					reportesDatamanager.setCedula(loginDatamanager.getUsuarioSession().getCedUsu());
					reportesDatamanager.setHabilitarCedula(Boolean.TRUE);
				}
				
			} else {
				reportesDatamanager.setSecComList(secComDTOInterface.findAll());
			}
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}
	
	public void generarFactura() throws Exception{
		
		String url =ConstantesUtil.CADENA_VACIA;
		usuarioDTO user=new usuarioDTO();
		
		Map<String, Object> parametros = new HashMap<String, Object>();  
		parametros.put(ReportServlet.TIPO_REPORTE, ReportServlet.REPORTE_PDF);
		parametros.put(ReportServlet.TIPO_SELECCIONADO, ReportServlet.TIPO_FACTURACION);
		parametros.put(ReportServlet.CONEXION, "si");
		parametros.put("fechDesde", reportesDatamanager.getFechaIni());
		parametros.put("fechHasta", reportesDatamanager.getFechaFin());
		parametros.put("idSector", reportesDatamanager.getSecComSelect().getIdSec());
		parametros.put("nomSector", secComDTOInterface.findById(reportesDatamanager.getSecComSelect().getIdSec()).getNomSec());
		if(!reportesDatamanager.getCedula().equals(ConstantesUtil.CADENA_VACIA)){
			user=usuarioDTOInterface.findByCedula(reportesDatamanager.getCedula());
		} else {
			if(reportesDatamanager.getTipReporte()==1){
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, "N�mero de c�dula es requerido para reporte detallado");
				return;
			}
		}
		
		if(reportesDatamanager.getTipReporte()==1){
			if(null!=user){
				parametros.put("cedula", reportesDatamanager.getCedula());
				parametros.put("nombres", user.getNomUsu());
				url = JsfUtil.getContextPath()+ "/reportServlet?reporte=reporteFacturacionDetallado";	
			} else {
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, "N�mero de c�dula no registrado.");
				return;
			}
			
		} else {
			if(!reportesDatamanager.getCedula().equals(ConstantesUtil.CADENA_VACIA)){
				if(null!=user){
					parametros.put("cedula", reportesDatamanager.getCedula());
					parametros.put("nombres", user.getNomUsu());
					url = JsfUtil.getContextPath()+ "/reportServlet?reporte=reporteUsuarioGeneral";	
				} else {
					JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, "N�mero de c�dula no registrado.");
					return;
				}
				
			} else {
				url = JsfUtil.getContextPath()+ "/reportServlet?reporte=reporteUsuarios";	
			}
		}
		
	
		HttpServletRequest request = JsfUtil.getRequest();
		request.getSession().setAttribute(ReportServlet.OBJETO_REPORTE, parametros);
		JsfUtil.getCurrentInstanceRequestContext().execute("window.open('" + url + "')");
	}

	public reportesDatamanager getReportesDatamanager() {
		return reportesDatamanager;
	}

	public void setReportesDatamanager(reportesDatamanager reportesDatamanager) {
		this.reportesDatamanager = reportesDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}
}
