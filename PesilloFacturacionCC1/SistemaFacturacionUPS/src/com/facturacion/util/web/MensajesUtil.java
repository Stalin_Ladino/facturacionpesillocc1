/*
 * Copyright 2013 Kruger
 * Todos los derechos reservados
 */ 
package com.facturacion.util.web;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Obtiene los mensajes del archivo de propiedades
 *  Creado: 15/09/2016
 * @author sladino
 *
 */
public class MensajesUtil {
	/**
	 * Archivo de propiedades
	 */
	private static final String BUNDLE_NAME = "com.gpf.resources.ApplicationResources";
	/**
	 * Permite leer el archivo de propiedades
	 */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	/**
	 * Permite Instanciar Mensajes
	 */
	private static final MensajesUtil INSTANCIA = new MensajesUtil();
	
	/**
	 * Retorna una instancia de la clase MensajesUtil
	 * 		@author sladino
	 * @return	instancia de la clase MensajesUtil
	 */
	public MensajesUtil getInstancia() {
		return INSTANCIA;
	}
	
	/**
	 * Permite obtener un valor string (key)
	 * @param key	Key del archivo de propiedades
	 * @return	Valor segun el key ingresado
	 * @throws MissingResourceException	En caso de encontrar el key respectivo
	 */
	public static String getString(String key) throws MissingResourceException{
		return RESOURCE_BUNDLE.getString(key);
	}
	
	/**
	 * Permite obtener un valor Integer (key)
	 * @param key	Key del archivo de propiedades
	 * @return	Valor (integer)segun el key ingresado
	 * @throws MissingResourceException	En caso de encontrar el key respectivo
	 */
	public static Integer getInteger(String key) throws MissingResourceException{
		return Integer.valueOf(RESOURCE_BUNDLE.getString(key));
	}
	/**
	 * Obtiene un mensaje del archivo de internacionalizaci&oacute;n con ciertos par&aacute;metros de relleno
	 * @param key Clave del archivo de internacionalizaci&oacute;n
	 * @param parameters Par&aacute;metros de relleno
	 * @return Mensaje obtenido desde el archivo de internacionalizaci&oacute;n con los par&aacute;metros establecidos
	 */
	public static String getString(final String key, final String... parameters) {
		String respuesta = null;
		try {
			final MessageFormat formatter = new MessageFormat(RESOURCE_BUNDLE.getString(key),RESOURCE_BUNDLE.getLocale());
			respuesta = formatter.format(parameters);
		} catch (MissingResourceException e) {
			respuesta = '!' + key + '!';
		}
		return respuesta;
	}	
}