package com.facturacion.util.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FactoryFinder;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.FileResolver;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.LocalJasperReportsContext;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * <b>Clase Utilizada para generar los reportes Jasper</b>
 * @author Stalin Ladino
 * @version 1.0
 * @since 1.6
 */


@WebServlet("/reportServlet")
public class ReportServlet extends HttpServlet{
	private static final long serialVersionUID = 8795000477609113780L;
//
	@Resource(mappedName = "java:jboss/datasources/appFact")
	private DataSource dataSource;
//
	public static final String SUB_REPORT_PREFIX = "";
	public static final String REPORT_PREFIX = "/page/reporte/";
	public static final String TIPO_SELECCIONADO = "TIPO_SELECCIONADO";
	public static final String CONEXION = "CONEXION";
	
	public static final String TIPO_FACTURACION= "facturacion/";
	
	public static final String REPORT_SUFFIX = ".jasper";
	public static final String REPORT_DEFINITION_SUFFIX = ".jrxml";
	public static final String OBJETO_REPORTE = "pprov_reportes";
	public static final String OBJETO_SUB_REPORTE = "pprov_subReportes";
	public static final String TIPO_REPORTE = "TIPO_REPORTE";
	public static final String NOMBRE_REPORTE = "NOMBRE_REPORTE";
	public static final String REPORTE_PDF = "pdf";
	public static final String REPORTE_TXT = "txt";
	public static final String REPORTE_ZIP = "zip";
	public static final String FILE = "file";
	public static final String USUARIO = "USUARIO";
	public static final String REPORTE_EXCEL = "excel";
	public static final String NOMBRE_REPORTE_DESCARGA = "NOMBRE_REPORTE_DESCARGA";
	public static final String INICIO_IN = "$X{IN,";
	public static final String INICIO_BETWEEN = "$X{[BETWEEN],";
	public static final String FINAL = "}";
	public static final String FILTRO_NULO = "1 = 1";
//
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String reporte = request.getParameter("reporte");
			String attachment = request.getParameter("inline");
			String tipoSeleccionado = request.getParameter("tipoSeleccionado") + "/";
			String tipoReporte = request.getParameter("tipoReporte");
			String reporteNombre = request.getParameter("reporteNombre");
			String conexion = request.getParameter("conexion");
//			File file = new File(request.getParameter("path"));
//			File file;
			
			
			//Mapa de parametros del reporte
			Map<String, Object> args = (HashMap<String, Object>) request.getSession().getAttribute(OBJETO_REPORTE);
			
			if (args == null) {
				args = new HashMap<String, Object>();
			}
			if(attachment == null){
				attachment = "inline";
			}
			//Nombre del archivo de reporte a generar
			if(args.containsKey(NOMBRE_REPORTE)){
				reporte = (String)args.get(NOMBRE_REPORTE);
				args.remove(NOMBRE_REPORTE);
			}
			//Nombre del archivo de reporte a generar
			if(args.containsKey(NOMBRE_REPORTE_DESCARGA)){
				reporteNombre = (String)args.get(NOMBRE_REPORTE_DESCARGA);
				args.remove(NOMBRE_REPORTE_DESCARGA);
			}
			//Se verifica el tipo de reporte SAO, SO, ROI o RESU
			if(args.containsKey(TIPO_SELECCIONADO)){
				tipoSeleccionado = (String)args.get(TIPO_SELECCIONADO);
				args.remove(TIPO_SELECCIONADO);
			}
			//Se verifica si llega el parametro tipo de reporte a generar
			if(args.containsKey(TIPO_REPORTE)){
				tipoReporte = (String)args.get(TIPO_REPORTE);
				args.remove(TIPO_REPORTE);
			}
			
			//Se verifica si llega el parametro tipo de reporte a generar
			if(args.containsKey(CONEXION)){
				conexion = (String)args.get(CONEXION);
				args.remove(CONEXION);
			}
			if(args.containsKey(FILE)){
				File file  = new File(args.get(FILE).toString());
				args.remove(FILE);
			
			byte[] byt = new byte[(int) file.length()];
	               FileInputStream fileInputStream = new FileInputStream(file);
	               fileInputStream.read(byt);
//	               for (int i = 0; i < b.length; i++) {
//	                           (char)b[i];
//	                }
	               response.setContentLength(byt.length);
	   			response.setContentType("application/zip");
	   			response.setHeader("Content-Disposition", attachment + ";filename=" + file.getName());
	               fileInputStream.close();
			response.getOutputStream().write(byt);
			
			}
			
			//Se compila los subreportes
			compilarSubReportes(request,tipoSeleccionado);
			//Se compila el reporte
			JasperReport report = compileReport(reporte,tipoSeleccionado);

			args.put("URL_LOGO", this.getServletContext().getRealPath(REPORT_PREFIX +"logo.png"));

			if ("excel".equals(tipoReporte)) {
				ByteArrayOutputStream outByte = new ByteArrayOutputStream();
				escribirXls(dataSource.getConnection(), outByte, reporte, report, args,tipoSeleccionado);
				byte[] bytes = outByte.toByteArray();
				response.setContentType("application/ms-excel");
				response.setHeader("Content-Disposition", "attachment;filename=" + reporte + ".xls;");
				response.setContentLength(bytes.length);
				response.getOutputStream().write(bytes);
			} else {
				if("pdf".equals(tipoReporte)){
					ByteArrayOutputStream outByte = new ByteArrayOutputStream();
					byte[] result =null;
					args.put("SUBREPORT_DIR", this.getServletContext().getRealPath(REPORT_PREFIX) + File.separator + tipoSeleccionado  );
					if(conexion.equals("si")){
						 result = JasperRunManager.runReportToPdf(report,args, dataSource.getConnection());
					} else {
						result = JasperRunManager.runReportToPdf(report, args, new JREmptyDataSource());
						JasperPrint jp = JasperFillManager.fillReport(report, args, new JREmptyDataSource());
						JRPdfExporter  exporter = new JRPdfExporter ();
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outByte);
			            exporter.exportReport();
					}
					response.setContentType("application/pdf");
					response.setContentLength(result.length);
					response.setHeader("Content-Disposition", attachment + ";filename=" + reporte + ".pdf;");
					response.getOutputStream().write(result);
				}else{
					if("txt".equals(tipoReporte)){
						ByteArrayOutputStream outByte = new ByteArrayOutputStream();
						escribirTxt(dataSource.getConnection(), outByte, reporte,  args,tipoSeleccionado);
						byte[] bytes = outByte.toByteArray();
						response.setContentType("application/text");
						if(null == reporteNombre){
							response.setHeader("Content-Disposition", attachment + ";filename=" + reporte + ".txt;");
						}else{
							response.setHeader("Content-Disposition", attachment + ";filename=" + reporteNombre + ".txt;");
						}
						response.setContentLength(bytes.length);
						
						response.getOutputStream().write(bytes);
					}
				}
			}
		} catch (JRException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();   
		}
	}

	private JasperReport compileReport(String reportName,String tipoSeleccionado) throws JRException {
		try {
			
			JasperReport report = null;
			String path = this.getServletContext().getRealPath(REPORT_PREFIX  + tipoSeleccionado + reportName + ".jasper");
			System.out.println("Ingresa a compilar reporte :" + path);
			File file = new File(path);
			FileInputStream fileStream = new FileInputStream(file);
			report = (JasperReport) JRLoader.loadObject(fileStream);
			return report;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private File compilarReporte(File file) throws JRException, IOException  {
		JasperDesign design = JRXmlLoader.load(file.getPath());
		File fileTmp = new File(file.getParent() + File.separator + file.getName().replaceAll(".jrxml", ".jasper"));
		fileTmp.createNewFile();
		OutputStream out = new FileOutputStream(fileTmp);
		JasperCompileManager.compileReportToStream(design, out);
		out.close();
		return fileTmp;
	}

	private void compilarSubReportes(HttpServletRequest request,String tipoSeleccionado) {
		String path = "";
		@SuppressWarnings("unchecked")
		Map<String, Object> subReportes = (HashMap<String, Object>) request.getSession().getAttribute(OBJETO_SUB_REPORTE);
		if (subReportes != null) {
			for (Object clave : subReportes.keySet()) {
				path = REPORT_PREFIX + tipoSeleccionado + clave + REPORT_DEFINITION_SUFFIX;
				String path2 = this.getServletContext().getRealPath(path);
				File file = new File(path2);
				try {
					file.createNewFile();
					compilarReporte(file);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private LocalJasperReportsContext getReportContext(final File file) {
		FileResolver resolver = new FileResolver() {
			@Override
			public File resolveFile(String filename) {
				return new File(file.getParent() + File.separator + filename);
			}
		};

		LocalJasperReportsContext ctx = new LocalJasperReportsContext(DefaultJasperReportsContext.getInstance());
		ctx.setClassLoader(getClass().getClassLoader());
		ctx.setFileResolver(resolver);
		return ctx;
	}

	public void escribirXls(Connection conn, ByteArrayOutputStream out, String clave, JasperReport jasper, Map<String, Object> parametros,String tipoSeleccionado) {
		try {
			String path = REPORT_PREFIX + tipoSeleccionado + clave + REPORT_SUFFIX;
			String path2 = this.getServletContext().getRealPath(path);
			JasperReport jasperReport =(JasperReport) JRLoader.loadObject(new FileInputStream(new File(path2)));
			JasperPrint jp = JasperFillManager.fillReport(jasperReport, parametros, conn);
			JRXlsExporter exporter = new JRXlsExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE);
			exporter.exportReport();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void escribirTxt(Connection conn, ByteArrayOutputStream out, String clave,  Map<String, Object> parametros,String tipoSeleccionado) {
		try {
			String path = REPORT_PREFIX + tipoSeleccionado + clave + REPORT_SUFFIX;
			String path2 = this.getServletContext().getRealPath(path);
			JasperReport jasperReport =(JasperReport) JRLoader.loadObject(new FileInputStream(new File(path2)));
			JasperPrint jp = JasperFillManager.fillReport(jasperReport, parametros, conn);
			JRTextExporter exporter = new JRTextExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
		    exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH, new Integer(250));
			exporter.exportReport();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected Object getManagedBean(String beanName, FacesContext facesContext) {
        return facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, beanName);
	}

	private abstract static class InnerFacesContext extends FacesContext {
		protected static void setFacesContextAsCurrentInstance(final FacesContext facesContext) {
			FacesContext.setCurrentInstance(facesContext);
		}
	}

	public FacesContext getFacesContext(final ServletRequest request, final ServletResponse response) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (facesContext == null) {
			FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
			LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
			Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
			
			ServletContext servletContext = ((HttpServletRequest) request).getSession().getServletContext();
			facesContext = contextFactory.getFacesContext(servletContext, request, response, lifecycle);
			InnerFacesContext.setFacesContextAsCurrentInstance(facesContext);
			 
			if (null == facesContext.getViewRoot()) {
				try{
		            facesContext.setViewRoot(new UIViewRoot());
				}catch (Exception e) {
					throw e;
				}
			}			
		}
		return facesContext;
	}

}

