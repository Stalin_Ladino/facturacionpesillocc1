package com.ec.gpf.contabilidad.controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;

import com.ec.gpf.adm.controlador.LoginController;
import com.ec.gpf.administracion.ejb.facade.AdBancosFacadeLocal;
import com.ec.gpf.administracion.ejb.facade.AdTiposCuentaFacadeLocal;
import com.ec.gpf.administracion.jpa.entidades.AdBancos;
import com.ec.gpf.administracion.jpa.entidades.AdTiposCuenta;
import com.ec.gpf.contabilidad.datamanager.cargaDepositoDatamanager;
import com.ec.gpf.fa.ejb.facade.FaTiposDepositosFacadeLocal;
import com.ec.gpf.fa.jpa.entidades.FaTiposDepositos;
import com.ec.gpf.ingresos.ejb.facade.InCargaDocumentosFacadeLocal;
import com.ec.gpf.ingresos.ejb.facade.InRegistroCargasDepFacadeLocal;
import com.ec.gpf.ingresos.ejb.facade.InRegistrosDepFarFacadeLocal;
import com.ec.gpf.ingresos.jpa.entidades.InRegistroCargasDep;
import com.ec.gpf.mom.datamanager.SessionDataManager;
import com.ec.gpf.util.CargaDepositoFunction;
import com.ec.gpf.util.ConstantesUtil;
import com.ec.gpf.util.UtilFacade;
import com.gpf.util.web.JsfUtil;
import com.gpf.util.web.MensajesUtil;
import com.gpf.util.web.ReportServlet;

@ManagedBean(name="cargaDepositoController")
@ViewScoped
public class CargaDepositoController extends CommonControlador{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2087718135470861044L;

	@EJB
    private InCargaDocumentosFacadeLocal inCargaDocumentosFacadeLocal;
	
	@EJB
    private FaTiposDepositosFacadeLocal faTiposDepositosFacadeLocal;
	
	@EJB
    private AdTiposCuentaFacadeLocal adTiposCuentaFacadeLocal;

	@EJB
	private AdBancosFacadeLocal adBancosFacadeLocal;
	
	@EJB
	private InRegistrosDepFarFacadeLocal inRegistrosDepFarFacadeLocal;
	
	
	
	@EJB
	private UtilFacade utilFacade;
	

	@ManagedProperty(value = "#{loginController}") 
	private LoginController loginController;
	
	
	@EJB
	private InRegistroCargasDepFacadeLocal inRegistroCargasDepFacadeLocal;
	
	@ManagedProperty(value = "#{sessionDataManager}") 
	private SessionDataManager sessionDataManager;
	
	@ManagedProperty(value = "#{cargaDepositoDatamanager}") 
	private cargaDepositoDatamanager cargaDepositoDatamanager;

	private Boolean init=Boolean.TRUE;
	
	public SessionDataManager getSessionDataManager() {
		return sessionDataManager;
	}

	public void setSessionDataManager(SessionDataManager sessionDataManager) {
		this.sessionDataManager = sessionDataManager;
	}


	public cargaDepositoDatamanager getCargaDepositoDatamanager() {
		return cargaDepositoDatamanager;
	}


	public void setCargaDepositoDatamanager(cargaDepositoDatamanager cargaDepositoDatamanager) {
		this.cargaDepositoDatamanager = cargaDepositoDatamanager;
	}

	@Override
	public void inicializarDatos() {
		
		if(init){
				
				cargaDepositoDatamanager.setFaTiposDepositos(new FaTiposDepositos());
				cargaDepositoDatamanager.setAdBancos(new AdBancos());
				cargaDepositoDatamanager.setAdTiposCuenta(new AdTiposCuenta());
				cargaDepositoDatamanager.setListFaTiposDepositos(faTiposDepositosFacadeLocal.findAll());
				cargaDepositoDatamanager.setAdTiposCuentaList(adTiposCuentaFacadeLocal.findAll());
				cargaDepositoDatamanager.setAdBancosList(adBancosFacadeLocal.findAll());
				cargaDepositoDatamanager.setFechaDesde(new Date());
				cargaDepositoDatamanager.setFechaHasta(new Date());
				cargaDepositoDatamanager.setListDepVenc(inCargaDocumentosFacadeLocal.
											cargarTablaDepoVenc(sessionDataManager.getIdEmpresa(),null,null));
				cargaDepositoDatamanager.setHablitarArchivo(Boolean.TRUE);
				cargaDepositoDatamanager.setMapRegistroCargasDep(new HashMap<String, InRegistroCargasDep>());
				init=Boolean.FALSE;
		}
	}
	
	/**
	 * Metodo ajax para habilitar carga de archivo
	 * @param event change al cambiar seleccion del combo
	 */
	public void validarCargaArchivo(AjaxBehaviorEvent event){
		if(null!=cargaDepositoDatamanager.getAdTiposCuenta().getCodigo() && 
				null!=cargaDepositoDatamanager.getAdBancos().getCodigo() &&
						(cargaDepositoDatamanager.getAdBancos().getCodigo().equals("14") ||
									cargaDepositoDatamanager.getAdBancos().getCodigo().equals("22"))){
			cargaDepositoDatamanager.setHablitarArchivo(Boolean.FALSE);
		} else {
			cargaDepositoDatamanager.setHablitarArchivo(Boolean.TRUE);
		}
	}
	
	
	
	public void loadArchivoDepositos(FileUploadEvent pFileUploadEvent) throws Exception {
		try {
			if (null == pFileUploadEvent.getFile() || pFileUploadEvent.getFile().getSize()<=0 ) {
				JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR, MensajesUtil.getString("etiqueta.error.archivo.vacio"));
				return;
			}
			cargaDepositoDatamanager.setInputStreamArchivoDespositos(pFileUploadEvent.getFile().getInputstream());
			importarArchivoClaves();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
	public void importarArchivoClaves() throws Exception {
		try {
			
			importarArchivoDepositos(cargaDepositoDatamanager.getInputStreamArchivoDespositos(),sessionDataManager.getIdEmpresa());
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void importarArchivoDepositos(InputStream inputStream, Long idOrg) throws SQLException, ParseException, NumberFormatException, IOException {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String linea = null;
			Integer cont=0;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			//Eliminar todos los datos de la tabla IN_REGISTRO_CARGAS_DEP
			//inRegistroCargasDepFacadeLocal.eliminarDatosTabla();
			
			while ((linea = br.readLine()) != null) {
				cont++;
				if(cont>10){
					InRegistroCargasDep archivoCargasDep = new InRegistroCargasDep();
					Pattern parseMen= Pattern.compile("(.*(\\t))(.*(\\t).*)((\\t).*)((\\t).*)((\\t).*)((\\t).*)((\\t).*)((\\t).*)((\\t).*)");
				    Matcher matcherMem = parseMen.matcher(linea);
				    
					 if (matcherMem.find()) {
						
					   archivoCargasDep.setUsuarioCarga(sessionDataManager.getUser());
					   archivoCargasDep.setCodigo(Long.parseLong(matcherMem.group(17).toString().trim()));
					   archivoCargasDep.setFechaCarga(formato.parse(matcherMem.group(1).toString()));
					   archivoCargasDep.setNumeroDepo(matcherMem.group(3).toString().trim());
					   archivoCargasDep.setTotalDepo(Double.parseDouble(matcherMem.group(13).toString().trim()));
					   archivoCargasDep.setFarmacia(ConstantesUtil.FARMACIA_SIN_NOMBRE);
					  
					   inRegistroCargasDepFacadeLocal.create(archivoCargasDep);
					   cargaDepositoDatamanager.getMapRegistroCargasDep().put(archivoCargasDep.getNumeroDepo(), archivoCargasDep);
					   } else{
					        if((linea = br.readLine()) != null && !(linea = br.readLine()).equals(ConstantesUtil.CADENA_VACIA)){
					        		JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR, MensajesUtil.getString("etiqueta.error.data.archivo"));
					        		return;
					        	 }
					       }
				}
			}
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, MensajesUtil.getString("etiqueta.exito.archivo.cargado"));
		} catch (Exception e) {
			System.out.println(e);
//			inRegistroCargasDepFacadeLocal.rollback();
			JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR, MensajesUtil.getString("etiqueta.error.carga.archivo"));
			return;
		}
	}

	/**
	 * Metodo para conciliar archivo
	 */
	public void validarCargaDeposito(){
		Double rest=0.0;
		for(CargaDepositoFunction listTabla:cargaDepositoDatamanager.getListDepVenc()){
			
			if(cargaDepositoDatamanager.getMapRegistroCargasDep().containsKey(listTabla.getDocumento())){
				cargaDepositoDatamanager.getMapRegistroCargasDep().get(listTabla.getDocumento());
				
			if(cargaDepositoDatamanager.getMapRegistroCargasDep().get(listTabla.getDocumento()).getTotalDepo()>=listTabla.getSaldo()){
				rest=cargaDepositoDatamanager.getMapRegistroCargasDep().get(listTabla.getDocumento()).getTotalDepo()-listTabla.getSaldo();
				listTabla.setSaldoCancelacion(rest);
				listTabla.setExisteDep(Boolean.TRUE);
			} 
			}
		}
	}
	
	/**
	 * Metodo para aceptar deposito
	 */
	public void aceptarCargaDeposito(){
		for(CargaDepositoFunction listTabla:cargaDepositoDatamanager.getListDepVenc()){
				if(cargaDepositoDatamanager.getMapRegistroCargasDep().containsKey(listTabla.getDocumento())){
					cargaDepositoDatamanager.getMapRegistroCargasDep().get(listTabla.getDocumento());
				if(listTabla.getExisteDep()){
					inRegistrosDepFarFacadeLocal.modificarEstado(listTabla.getDocumento(),"C");
				} else {
					inRegistrosDepFarFacadeLocal.modificarEstado(listTabla.getDocumento(),"P");
				}
			}
		}
		
		cargaDepositoDatamanager.setListDepVenc(inCargaDocumentosFacadeLocal.cargarTablaDepoVenc(sessionDataManager.getIdEmpresa(),null,null));
	}
	
	/**
	 * Metodo para generar reporte excel
	 * @throws IOException
	 * @throws ParseException 
	 */
	public void generarReporteCargaDeposito() throws IOException, ParseException{
//		ge();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Object> parametros = new HashMap<String, Object>();  
		parametros.put(ReportServlet.TIPO_REPORTE, ReportServlet.REPORTE_EXCEL);
		parametros.put(ReportServlet.TIPO_SELECCIONADO, ReportServlet.TIPO_CONTABILIDAD);
		parametros.put("tipoDep", cargaDepositoDatamanager.getTipoDep()!=null?
									cargaDepositoDatamanager.getTipoDep():null);
		parametros.put("fechaDesde", formato.format(cargaDepositoDatamanager.getFechaDesde()).toString()+" 00:00:00");
		parametros.put("fechaHasta", formato.format(cargaDepositoDatamanager.getFechaHasta()).toString()+" 23:59:00");
//		parametros.put("nomSucursal", sessionDataManager.getAdSucursale().getNombre());
		
		HttpServletRequest request = JsfUtil.getRequest();
		request.getSession().setAttribute(ReportServlet.OBJETO_REPORTE, parametros);
	}
	
//	 public void ge() {
//		  HashMap map = new HashMap();
//		  map.put("iniciadorNombre", "abc");
//		  map.put("numRegistro", "123456789");
//		  List<String> list = new ArrayList<String>();
//		  list.add("stalin")
//		  try {
//		   ReportUtil.exportarReportePDF("C:\\Users\\Usuario\\Desktop\\cargaDepositos_List.jrxml", map, "listafactura.pdf", list);
//		  } catch (Exception e) {
//		   e.printStackTrace();
//		  }
//		 }
	
	
	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	

}
