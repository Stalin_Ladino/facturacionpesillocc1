package com.ec.gpf.contabilidad.controlador;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import com.ec.gpf.addressBook.ejb.facade.AbPersonaFacadeLocal;
import com.ec.gpf.addressBook.jpa.entidades.AbPersona;
import com.ec.gpf.administracion.ejb.facade.AdEmpresaFacadeLocal;
import com.ec.gpf.administracion.ejb.facade.AdImpresoraFacadeLocal;
import com.ec.gpf.administracion.jpa.entidades.AdEmpresa;
import com.ec.gpf.administracion.jpa.entidades.AdImpresora;
import com.ec.gpf.ingresos.ejb.facade.InParametrosFacadeLocal;
import com.ec.gpf.ingresos.ejb.facade.InParametrosMasFacadeLocal;
import com.ec.gpf.ingresos.ejb.facade.InParametrosSucursalFacadeLocal;
import com.ec.gpf.ingresos.jpa.entidades.InParametro;
import com.ec.gpf.ingresos.jpa.entidades.InParametrosMa;
import com.ec.gpf.ingresos.jpa.entidades.InParametrosSucursal;
import com.ec.gpf.ingresos.jpa.entidades.InParametrosSucursalPK;
import com.ec.gpf.mom.datamanager.SessionDataManager;
import com.ec.gpf.util.ConstantesUtil;
import com.gpf.util.web.JsfUtil;
import com.gpf.util.web.MensajesUtil;

@ManagedBean(name = "parametrosGeneralesController")
@ViewScoped
public class ParametrosGeneralesController {

	private int valor;
	private List<InParametro> listInParametro;

	private String impresora;
	private List<AdImpresora> listImpresoras;
	private List<SelectItem> listaImpresoras;

	private InParametro inParametro = new InParametro();
	private InParametrosMa inParametroMa = new InParametrosMa();
	private InParametrosSucursal inParametrosSucursal = new InParametrosSucursal();
	private InParametrosSucursalPK inParametrosSucursalPk = new InParametrosSucursalPK();
	private AdEmpresa adEmpresa = new AdEmpresa();
	private AbPersona abPersona = new AbPersona();

	@EJB
	private InParametrosFacadeLocal ejbInParametro;
	@EJB
	private InParametrosMasFacadeLocal ejbInParametroMa;
	@EJB
	private InParametrosSucursalFacadeLocal ejbInParametroSucursal;
	@EJB
	private AdImpresoraFacadeLocal ejbImpresora;
	@EJB
	private AdEmpresaFacadeLocal ejbEmpresa;
	@EJB
	private AbPersonaFacadeLocal ejbPersona;

	@ManagedProperty(value = "#{sessionDataManager}")
	private SessionDataManager sessionDataManager;

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public InParametro getInParametro() {
		return inParametro;
	}

	public void setInParametro(InParametro inParametro) {
		this.inParametro = inParametro;
	}

	public InParametrosMa getInParametroMa() {
		return inParametroMa;
	}

	public void setInParametroMa(InParametrosMa inParametroMa) {
		this.inParametroMa = inParametroMa;
	}

	public InParametrosSucursal getInParametrosSucursal() {
		return inParametrosSucursal;
	}

	public void setInParametrosSucursal(
			InParametrosSucursal inParametrosSucursal) {
		this.inParametrosSucursal = inParametrosSucursal;
	}

	public InParametrosSucursalPK getInParametrosSucursalPk() {
		return inParametrosSucursalPk;
	}

	public void setInParametrosSucursalPk(
			InParametrosSucursalPK inParametrosSucursalPk) {
		this.inParametrosSucursalPk = inParametrosSucursalPk;
	}

	public AdEmpresa getAdEmpresa() {
		return adEmpresa;
	}

	public void setAdEmpresa(AdEmpresa adEmpresa) {
		this.adEmpresa = adEmpresa;
	}

	public AbPersona getAbPersona() {
		return abPersona;
	}

	public void setAbPersona(AbPersona abPersona) {
		this.abPersona = abPersona;
	}

	public SessionDataManager getSessionDataManager() {
		return sessionDataManager;
	}

	public void setSessionDataManager(SessionDataManager sessionDataManager) {
		this.sessionDataManager = sessionDataManager;
	}

	public List<InParametro> getListInParametro() {
		return listInParametro;
	}

	public void setListInParametro(List<InParametro> listInParametro) {
		this.listInParametro = listInParametro;
	}

	public String getImpresora() {
		return impresora;
	}

	public void setImpresora(String impresora) {
		this.impresora = impresora;
	}

	public List<AdImpresora> getListImpresoras() {
		return listImpresoras;
	}

	public void setListImpresoras(List<AdImpresora> listImpresoras) {
		this.listImpresoras = listImpresoras;
	}

	public List<SelectItem> getListaImpresoras() {
		return listaImpresoras;
	}

	public void setListaImpresoras(List<SelectItem> listaImpresoras) {
		this.listaImpresoras = listaImpresoras;
	}

	@PostConstruct
	public void init() {
		inParametro.setBodega(false);
		inParametro.setVariosBancos(false);
		adEmpresa.setContribuyenteEspecial(false);
		inParametrosSucursal.setImprimeRecap(false);
		inParametrosSucursal.setGeneraInterface(false);
	}

	public void abreDialogo() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('seleccionEmpresa').show();");

	}

	public void guardarParametros() {

		try {
			ejbEmpresa.edit(adEmpresa);
		} catch (Exception e) {
			throw e;
		}
		try {
			ejbInParametro.edit(inParametro);
		} catch (Exception e) {
			throw e;
		}

		try {
			ejbInParametroMa.edit(inParametroMa);
		} catch (Exception e) {
			throw e;
		}
		try {
			ejbInParametroSucursal.edit(inParametrosSucursal);
		} catch (Exception e) {
			throw e;
		}
		mensaje();
	}

	public void mensaje() {
		 JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, MensajesUtil.getString("etiqueta.mensaje.almacenamiento.correcto"));
	}

	public void inicializarVariables() {

		inParametro = new InParametro();
		inParametroMa = new InParametrosMa();
		inParametrosSucursal = new InParametrosSucursal();
		inParametrosSucursalPk = new InParametrosSucursalPK();
		adEmpresa = new AdEmpresa();
		abPersona = new AbPersona();

		listaImpresoras = new ArrayList<SelectItem>();
		// Uso de variables de sesion para identificar la empresa y sucursal
		// seleccionados.
		inParametrosSucursalPk.setEmpresa(sessionDataManager.getIdEmpresa());
		inParametrosSucursalPk.setSucursal(sessionDataManager.getIdSucursal());

		inParametro = ejbInParametro.find(sessionDataManager.getIdEmpresa());
		inParametroMa = ejbInParametroMa
				.find(sessionDataManager.getIdEmpresa());
		inParametrosSucursal = ejbInParametroSucursal
				.find(inParametrosSucursalPk);
		adEmpresa = ejbEmpresa.find(sessionDataManager.getIdEmpresa());
		abPersona = ejbPersona.find(sessionDataManager.getIdEmpresa());
		listImpresoras = ejbImpresora.findAll();

		for (AdImpresora ai : listImpresoras) {
			listaImpresoras.add(new SelectItem(ai.getDispositivo(), ai
					.getDispositivo()));
		}
	}
}
