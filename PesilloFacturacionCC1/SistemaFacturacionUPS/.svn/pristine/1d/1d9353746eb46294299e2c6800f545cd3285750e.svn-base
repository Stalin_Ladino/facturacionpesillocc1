/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.gpf.contabilidad.datamanager;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.gpf.administracion.jpa.entidades.AdBancos;
import com.ec.gpf.administracion.jpa.entidades.AdTiposCuenta;
import com.ec.gpf.fa.jpa.entidades.FaTiposDepositos;
import com.ec.gpf.ingresos.jpa.entidades.InRegistroCargasDep;
import com.ec.gpf.ingresos.jpa.entidades.InVencimientoDepositos;
import com.ec.gpf.util.CargaDepositoFunction;

/**
 * Guarda los datos de sesion del usuario logueado
 * @author sladino
 *
 */
@ManagedBean(name="cargaDepositoDatamanager")
@SessionScoped
public class cargaDepositoDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	
	/**
	 * Lista con datos de vencimiento de despositos
	 */
	private List<CargaDepositoFunction> listDepVenc;
	
	/**
	 * Lista con datos de vencimiento seleccionados
	 */
	private List<InVencimientoDepositos> listSelectDepVenc;
	
	/**
	 * Lista con datos de vencimiento seleccionados
	 */
	private List<FaTiposDepositos> listFaTiposDepositos;
	
	/**
	 * Objeto para guardar combo seleccionado
	 */
	private FaTiposDepositos faTiposDepositos;
	
	/**
	 * Lista con datos de vencimiento seleccionados
	 */
	private List<AdTiposCuenta> adTiposCuentaList;
	
	/**
	 * Objeto para guardar combo seleccionado
	 */
	private AdTiposCuenta adTiposCuenta;
	
	/**
	 * Lista con datos de bancos
	 */
	private List<AdBancos> adBancosList;
	
	/**
	 * Objeto para guardar combo seleccionado
	 */
	private AdBancos adBancos;
	
	/**
	 * Fecha desde para seleccionar reporte
	 */
	private Date fechaDesde;
	/**
	 * Fecha desde para seleccionar reporte
	 */
	private Date fechaHasta;
	
	/**
	 * Variable para habilitar seleccion de archivo
	 */
	private Boolean hablitarArchivo;
	
	/**
	 * Archivos que contienen la carga de depositos
	 */
	private InputStream inputStreamArchivoDespositos;
	
	/**
	 * Archivo cargado
	 */
	private List<InRegistroCargasDep>ListRegistroCargasDep;
	
	
	/**
	 * Archivo cargado
	 */
	private HashMap<String,InRegistroCargasDep>MapRegistroCargasDep = new HashMap<String, InRegistroCargasDep>();
	
	
	/**
	 * Variable para almacenar tipo deposito
	 */
	private Integer tipoDep;
	
	public Integer getTipoDep() {
		return tipoDep;
	}

	public void setTipoDep(Integer tipoDep) {
		this.tipoDep = tipoDep;
	}

	public List<CargaDepositoFunction> getListDepVenc() {
		return listDepVenc;
	}

	public void setListDepVenc(List<CargaDepositoFunction> listDepVenc) {
		this.listDepVenc = listDepVenc;
	}

	public List<InVencimientoDepositos> getListSelectDepVenc() {
		return listSelectDepVenc;
	}

	public void setListSelectDepVenc(List<InVencimientoDepositos> listSelectDepVenc) {
		this.listSelectDepVenc = listSelectDepVenc;
	}

	public FaTiposDepositos getFaTiposDepositos() {
		return faTiposDepositos;
	}

	public void setFaTiposDepositos(FaTiposDepositos faTiposDepositos) {
		this.faTiposDepositos = faTiposDepositos;
	}

	public List<FaTiposDepositos> getListFaTiposDepositos() {
		return listFaTiposDepositos;
	}

	public void setListFaTiposDepositos(List<FaTiposDepositos> listFaTiposDepositos) {
		this.listFaTiposDepositos = listFaTiposDepositos;
	}

	public List<AdTiposCuenta> getAdTiposCuentaList() {
		return adTiposCuentaList;
	}

	public void setAdTiposCuentaList(List<AdTiposCuenta> adTiposCuentaList) {
		this.adTiposCuentaList = adTiposCuentaList;
	}

	public AdTiposCuenta getAdTiposCuenta() {
		return adTiposCuenta;
	}

	public void setAdTiposCuenta(AdTiposCuenta adTiposCuenta) {
		this.adTiposCuenta = adTiposCuenta;
	}

	public List<AdBancos> getAdBancosList() {
		return adBancosList;
	}

	public void setAdBancosList(List<AdBancos> adBancosList) {
		this.adBancosList = adBancosList;
	}

	public AdBancos getAdBancos() {
		return adBancos;
	}

	public void setAdBancos(AdBancos adBancos) {
		this.adBancos = adBancos;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Boolean getHablitarArchivo() {
		return hablitarArchivo;
	}

	public void setHablitarArchivo(Boolean hablitarArchivo) {
		this.hablitarArchivo = hablitarArchivo;
	}

	public InputStream getInputStreamArchivoDespositos() {
		return inputStreamArchivoDespositos;
	}

	public void setInputStreamArchivoDespositos(
			InputStream inputStreamArchivoDespositos) {
		this.inputStreamArchivoDespositos = inputStreamArchivoDespositos;
	}

	public List<InRegistroCargasDep> getListRegistroCargasDep() {
		return ListRegistroCargasDep;
	}

	public void setListRegistroCargasDep(
			List<InRegistroCargasDep> listRegistroCargasDep) {
		ListRegistroCargasDep = listRegistroCargasDep;
	}

	public HashMap<String, InRegistroCargasDep> getMapRegistroCargasDep() {
		return MapRegistroCargasDep;
	}

	public void setMapRegistroCargasDep(
			HashMap<String, InRegistroCargasDep> mapRegistroCargasDep) {
		MapRegistroCargasDep = mapRegistroCargasDep;
	}
	
	
	
}
