package com.gpf.util.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.service.estructura.responseEstructure;

public class ServicioUtil {

	private final static ServicioUtil INSTANCIA = new ServicioUtil();

	private ServicioUtil() {

	}

	public static ServicioUtil getInstancia() {
		return INSTANCIA;
	}

	public Map<String, Object> convertJsonData(JSONObject jsonData) throws JSONException {
		
		Map<String, Object> atributoValor = new HashMap<String, Object>();

		for (Iterator<?> iterator = jsonData.keys(); iterator.hasNext();) {

			// La llave
			String key = (String) iterator.next();

			// Extrae los datos del Json y los pone en un mapa
			atributoValor.put(key, jsonData.get(key));

		}
		return atributoValor;
	}

	public List<Map<String, Object>> convertJsonArrayData(String jsonData, String nameArray) throws JSONException, Exception {
		List<Map<String, Object>> dataArray = new ArrayList<Map<String, Object>>();
		JSONObject jsonObject = new JSONObject(jsonData);
		JSONArray jsonArray = jsonObject.getJSONArray(nameArray);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject explrObject = jsonArray.getJSONObject(i);
			Map<String, Object> data = convertJsonData(explrObject);
			dataArray.add(data);
		}
		if(dataArray.size()<=0){
			//throw new ParkingZoneException(MensajesUtil.getString("mensagge.error.empty.json"));
		}
		return dataArray;
		
	}
	public List<Map<String, Object>> convertJsonArrayDataList(String jsonData, String nameArray) throws JSONException {
		List<Map<String, Object>> dataArray = new ArrayList<Map<String, Object>>();
		JSONObject jsonObject = new JSONObject(jsonData);
		JSONArray jsonArray = jsonObject.getJSONArray(nameArray);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject explrObject = jsonArray.getJSONObject(i);
			Map<String, Object> data = convertJsonData(explrObject);
			dataArray.add(data);
		}
		return dataArray;
	}

	/**
	 * Permite crear un objeto de tipo LoginDTO a partir del mapa de atributos
	 * de login que llegan
	 * 
	 * @param atributoLogin,
	 *            atributos de login
	 * @return LoginDTO un objeto login con todos los atributos correspondientes
	 *         seteados
	 */
	
	/**
	 * Permite generar el objeto SessionDTO a partir de un JSON
	 * 
	 * @param atributosSession,
	 *            trama JSON que se desea transformar a objeto
	 * @return SessionDTO con todos los atributos enviados en la trama JSON
	 */
	public loginDTO createloginDTOInstance(Map<String, Object> atributosSession) {
		loginDTO login = new loginDTO();
		login.setIdLogin((Integer) atributosSession.get("idLogin"));
		login.setNomUser((String) atributosSession.get("user"));
		login.setPassUser((String) atributosSession.get("pass"));
		return login;
	}
/**
 * Metodo para convertir el sector a Json
 * @param atributosSession
 * @return
 */
	public secComDTO createSectorDTOInstance(Map<String, Object> atributosSession) {
		secComDTO sector = new secComDTO();
		sector.setIdSec((Integer) atributosSession.get("idSector"));
		return sector;
	}

	/**
	 * Permite armar la trama json de respuesta con todos los datos que le
	 * llegan
	 * 
	 * @param responseEstructure,
	 *            es la estructura establecida para el response que contiene el
	 *            codigo ya sea 1 en caso de exito o 0 en caso de error, el
	 *            mensaje en caso de que exista un mensaje de error, data en
	 *            caso de que se tenga datos que se deseen retornar
	 * @return trama json con la estructura result, message, data
	 * @throws JSONException,en
	 *             caso de que ocurra una excepcion
	 */
	public String createJSONDataResponse(responseEstructure responseEstructure, JSONArray data) throws JSONException {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("status", responseEstructure.getStatus());
		jsonResponse.put("message", responseEstructure.getMessage());

		if(null!=responseEstructure.getData() && (data == null ||data.length()==0)) {
			jsonResponse.put("data", responseEstructure.getData());
			} else {
			if (data != null)
				jsonResponse.put("data", data); 
			else
				jsonResponse.put("data", new ArrayList<Object>());
			}

		return jsonResponse.toString();
	}

	/**
	 * Permite armar la trama json de respuesta con todos los datos que le
	 * llegan
	 * 
	 * @param responseEstructure,
	 *            ees la estrucutra establecida para el response que contiene el
	 *            codigo ya sea 1 en caso de exito o 0 en caso de error, el
	 *            mensaje en caso de que exista un mensaje de error, data en
	 *            caso de que tenga datos para retornar
	 * @return trama json con al estructura result, message, data
	 * @throws JSONException,
	 *             en caso de que ocurra una excepcion
	 */
	public String createJSONDataResponse(responseEstructure responseEstructure) throws JSONException {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("status", responseEstructure.getStatus());
		jsonResponse.put("message", responseEstructure.getMessage());
		jsonResponse.put("data", responseEstructure.getSingleData());
		return jsonResponse.toString();
	}
	
	/**
	 * Permite generar el objeto de Consumo
	 * 
	 * @param atributosSession,
	 *            trama JSON que se desea transformar a objeto
	 * @return Consumo Obj con todos los atributos enviados en la trama JSON
	 */
	public TblConsumo createTblConsumoInstance(Map<String, Object> atributosSession) {
		TblConsumo consumo = new TblConsumo();
		consumo.setIdCodMed((String) atributosSession.get("codMedidor"));
		String valor= (String)atributosSession.get("totConsumo");
		String validacion=valor.replace(",",".");
		consumo.setTotConsum(Double.parseDouble(validacion.toString()));
		consumo.setIdSec((Integer) atributosSession.get("idSector"));
		return consumo;
	}
	
	
}
