package com.ec.ups.service.data;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;

import com.ec.ups.jpa.entidades.secComDTO;


public abstract class ConvertFromServiceData<T> {

	protected String jsonData;

	public abstract T convert() throws Exception;

	public abstract Collection<T> convertToCollection() throws Exception, JSONException, Exception;

	public abstract JSONArray convertFromCollectionToJson(Collection<T> collection) throws JSONException;
	
	
	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public JSONArray convertFromCollectionToUserProfileZones(
			Collection<Integer> collection) throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}


	public secComDTO convertSector() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
}
