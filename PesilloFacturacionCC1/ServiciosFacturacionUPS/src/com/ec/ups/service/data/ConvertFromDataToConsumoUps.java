package com.ec.ups.service.data;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ec.ups.jpa.entidades.TblConsumo;
import com.gpf.util.web.ServicioUtil;

public class ConvertFromDataToConsumoUps extends ConvertFromServiceData<TblConsumo> {

	private static final ConvertFromDataToConsumoUps INSTANCIA = new ConvertFromDataToConsumoUps();

	public static ConvertFromDataToConsumoUps getInstancia() {

		return INSTANCIA;
	}

	public static ConvertFromDataToConsumoUps getInstancia(String jsonData) {

		INSTANCIA.setJsonData(jsonData);
		return INSTANCIA;
	}

	private ConvertFromDataToConsumoUps() {

	}

	public TblConsumo convertToSession() throws JSONException {
		JSONObject jsonData = new JSONObject(this.jsonData);
		return ServicioUtil.getInstancia()
				.createTblConsumoInstance(ServicioUtil.getInstancia().convertJsonData(jsonData));

	}


	@Override
	public TblConsumo convert() throws Exception {
		try {
			return convertToSession();
		} catch (Exception e) {
//			e.printStackTrace();
			//throw new Exception(MensajesUtil.getString("mensagge.error.estructure.json"),ResultEnum.ERROR.getValor());
		}
		return null;
	}
	
	@Override
	public Collection<TblConsumo> convertToCollection() throws Exception {
		return null;
	}

	private JSONObject createconsumoData(TblConsumo consumo) throws JSONException {
		JSONObject sessionJSON = new JSONObject();
		
		sessionJSON.put("medidor", consumo.getIdCodMed());
		sessionJSON.put("fecha", consumo.getFechaConsumo());
		sessionJSON.put("nomUser", consumo.getNomUser());
		sessionJSON.put("cedUser", consumo.getCedUser());

		return sessionJSON;
	}

	@Override
	public JSONArray convertFromCollectionToJson(Collection<TblConsumo> collection) throws JSONException {
		JSONArray sessionArray = new JSONArray();
		if (collection != null && !collection.isEmpty()) {
			for (TblConsumo session : collection) {
				sessionArray.put(createconsumoData(session));
			}
		}
		return sessionArray;
	}


	
}
