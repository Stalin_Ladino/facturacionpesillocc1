package com.ec.ups.service.data;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ec.ups.jpa.entidades.loginDTO;
import com.gpf.util.web.ServicioUtil;

public class ConvertFromDataToLoginUps extends ConvertFromServiceData<loginDTO> {

	private static final ConvertFromDataToLoginUps INSTANCIA = new ConvertFromDataToLoginUps();

	public static ConvertFromDataToLoginUps getInstancia() {

		return INSTANCIA;
	}

	public static ConvertFromDataToLoginUps getInstancia(String jsonData) {

		INSTANCIA.setJsonData(jsonData);
		return INSTANCIA;
	}

	private ConvertFromDataToLoginUps() {

	}

	/**
	 * Permite crear un loginDTO a partir de una trama JSON enviada
	 * 
	 * @return loginDTO entidad JSON con los datos enviados en la trama json
	 * @throws JSONException,
	 *             en caso de que ocurra una excepcion
	 */
	public loginDTO convertToSession() throws JSONException {
		JSONObject jsonData = new JSONObject(this.jsonData);
		return ServicioUtil.getInstancia()
				.createloginDTOInstance(ServicioUtil.getInstancia().convertJsonData(jsonData));

	}


	@Override
	public loginDTO convert() throws Exception {
		try {
			return convertToSession();
		} catch (Exception e) {
//			e.printStackTrace();
			//throw new Exception(MensajesUtil.getString("mensagge.error.estructure.json"),ResultEnum.ERROR.getValor());
		}
		return null;
	}
	
	@Override
	public Collection<loginDTO> convertToCollection() throws Exception {
		return null;
	}

	/**
	 * Permite generar una trama json a partir de una entidad Session
	 * 
	 * @param session,
	 *            entidad que se desea parsear a trama json
	 * @return JsonObject, es la trama json que respresenta a la entidad session
	 * @throws JSONException,
	 *             en caso de que exista una excepcion
	 */
	private JSONObject createLoginData(loginDTO login) throws JSONException {
		JSONObject sessionJSON = new JSONObject();
		sessionJSON.put("id", login.getIdLogin());
		sessionJSON.put("userName", login.getNomUser());
		return sessionJSON;
	}

	@Override
	public JSONArray convertFromCollectionToJson(Collection<loginDTO> collection) throws JSONException {
		JSONArray sessionArray = new JSONArray();
		if (collection != null && !collection.isEmpty()) {
			for (loginDTO session : collection) {
				sessionArray.put(createLoginData(session));
			}
		}
		return sessionArray;
	}


	
}
