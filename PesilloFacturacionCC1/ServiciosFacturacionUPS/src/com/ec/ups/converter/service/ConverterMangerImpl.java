package com.ec.ups.converter.service;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;

import com.ec.ups.service.data.ConvertFromServiceData;

public class ConverterMangerImpl<T> implements ConverterManager<T> {

	private ConvertFromServiceData<T> converter;

	@SuppressWarnings("rawtypes")
	private static final ConverterMangerImpl<?> INSTANCIA = new ConverterMangerImpl();

	@SuppressWarnings("rawtypes")
	public static ConverterMangerImpl getInstancia() {
		return INSTANCIA;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object convert(ConvertFromServiceData converter) throws Exception {
		this.converter = converter;
		Object convertedType = converter.convert();
		return convertedType;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Collection convertToCollection(ConvertFromServiceData converter) throws Exception {
		this.converter = converter;
		return converter.convertToCollection();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ConvertFromServiceData getConverter() {
		return converter;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public JSONArray convertFromCollectionToJson(ConvertFromServiceData converter, Collection collection)
			throws JSONException {
		this.converter = converter;
		JSONArray convertedType = converter.convertFromCollectionToJson(collection);
		return convertedType;
	}

	@Override
	public Collection<T> convertToCollectionType(
			ConvertFromServiceData<?> converter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray convertFromCollectionToJsonCount(
			ConvertFromServiceData<?> converter, Collection<T> collection)
			throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray convertFromCollectionToJsonType(
			ConvertFromServiceData<?> converter, Collection<T> collection)
			throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray convertFromCollectionToJsonDateTime(
			ConvertFromServiceData converter, Collection collection)
			throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONArray convertFromCollectionToJsonAccess(
			ConvertFromServiceData converter, Collection collection)
			throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}
}



